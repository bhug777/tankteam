DROP TABLE valeurCase;
DROP TABLE spawn;
DROP TABLE mapTT;

DROP SEQUENCE ma_seq_map;
DROP SEQUENCE ma_seq_case;
DROP SEQUENCE seq_spawn;

create sequence ma_seq_map 
start with 1 
increment by 1 
nomaxvalue; 

create sequence ma_seq_case
start with 1 
increment by 1 
nomaxvalue; 

create sequence seq_spawn
start with 1 
increment by 1 
nomaxvalue; 

CREATE TABLE mapTT(
	id 					INTEGER DEFAULT ma_seq_map.NEXTVAL PRIMARY KEY,
	nbX 				INTEGER,
	nbY 				INTEGER,
	etat 				VARCHAR(10),/*Public, prive...*/
	nom 				VARCHAR(20),
	tempsMin 			INTEGER,
	tempsMax 			INTEGER
);

CREATE TABLE valeurCase(
	id 					INTEGER DEFAULT ma_seq_case.NEXTVAL PRIMARY KEY,
	id_mapTT 			INTEGER,
	posx 				INTEGER,
	posy 				INTEGER,
	valeur 				INTEGER,
	
	FOREIGN KEY (id_mapTT)REFERENCES mapTT(id)
);


CREATE TABLE spawn(
	id 					INTEGER DEFAULT seq_spawn.NEXTVAL PRIMARY KEY,
	idniveau 			INTEGER 		NOT NULL,
	posx 				INTEGER 		NOT NULL,
	posy 				INTEGER 		NOT NULL,

	CONSTRAINT idspawn_fk FOREIGN KEY(idniveau) REFERENCES mapTT(id) ON DELETE CASCADE
);

