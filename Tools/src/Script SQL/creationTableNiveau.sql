CREATE SEQUENCE seq_niveau START WITH 1 INCREMENT BY 1 ;
CREATE SEQUENCE seq_tuile START WITH 1 INCREMENT BY 1 ;
CREATE SEQUENCE seq_spawn START WITH 1 INCREMENT BY 1 ;

CREATE TABLE niveau(
	id 					INTEGER DEFAULT seq_niveau.NEXTVAL,
	titre 				VARCHAR2(50) 	NOT NULL,
	statut 				varchar2(20) 	not null check (statut in ('Public','Equipe','Prive','Inactif')),
	dateDeCreation 		DATE 			NOT NULL,
	delaiApparitionMin 	FLOAT 			NOT NULL,
	delaiApparitionMax 	FLOAT 			NOT NULL,
	dimensionX 			INT 			NOT NULL CHECK(dimensionX BETWEEN 6 AND 12),
	dimensionY 			INT 			NOT NULL CHECK(dimensionY BETWEEN 6 AND 12),

	CONSTRAINT idniveau_pk PRIMARY KEY (id)
);

CREATE TABLE tuile(
	id 					INTEGER DEFAULT seq_tuile.NEXTVAL PRIMARY KEY,
	idniveau 			INTEGER 		NOT NULL,
	typeTuile 			varchar2(20) 	check(typeTuile in('Mur','Mav','Mai')),
	posx 				INT 			NOT NULL,
	posy 				INT 			NOT NULL,

	CONSTRAINT idniveau_fk FOREIGN KEY(idniveau) REFERENCES niveau(id) ON DELETE CASCADE
);

CREATE TABLE spawn(
	id 					INTEGER DEFAULT seq_spawn.NEXTVAL PRIMARY KEY,
	idniveau 			INTEGER 		NOT NULL,
	posx 				INTEGER 		NOT NULL,
	posy 				INTEGER 		NOT NULL,


	CONSTRAINT idspawn_fk FOREIGN KEY(idniveau) REFERENCES niveau(id) ON DELETE CASCADE
);


INSERT INTO niveau VALUES(seq_niveau.nextval,'small','Public',SYSDATE,5,15,6,6);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',3,3);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',2,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',2,6);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),2,2);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),4,2);

INSERT INTO niveau VALUES(seq_niveau.nextval,'El Publico','Public',SYSDATE,5,20,8,6);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mav',3,3);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',2,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',1,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',6,6);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',2,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mai',2,6);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),2,2);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),4,2);

INSERT INTO niveau VALUES(seq_niveau.nextval,'El Provido','Prive',SYSDATE,5,5,12,12);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mav',3,3);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mav',2,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',4,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',10,6);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mai',2,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mai',11,6);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),1,1);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),6,6);

INSERT INTO niveau VALUES(seq_niveau.nextval,'Noice','Prive',SYSDATE,5,5,7,7);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mav',3,3);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mav',6,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',4,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',1,6);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mai',2,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mai',7,6);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),1,1);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),6,6);

INSERT INTO niveau VALUES(seq_niveau.nextval,'Equipou','Equipe',SYSDATE,5,5,11,7);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mav',11,3);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mav',6,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',4,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',1,6);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mai',2,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mai',11,6);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),11,7);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),6,6);

INSERT INTO niveau VALUES(seq_niveau.nextval,'ptite equipe','Equipe',SYSDATE,5,5,9,6);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mav',9,3);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mav',6,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',9,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',1,6);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mai',7,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mai',7,6);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),1,1);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),9,6);

INSERT INTO niveau VALUES(seq_niveau.nextval,'Noice','Inactif',SYSDATE,5,5,6,6);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mav',3,3);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mav',6,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',4,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mur',1,6);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mai',2,1);
INSERT INTO tuile VALUES(seq_tuile.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),'Mai',6,6);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),4,4);
INSERT INTO spawn VALUES(seq_spawn.nextval,(SELECT id from niveau where rowid=(select max(rowid)from niveau)),2,2);

COMMIT