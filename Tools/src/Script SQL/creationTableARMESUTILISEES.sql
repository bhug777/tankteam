DROP TABLE ARMEUTILISEES;

CREATE TABLE ARMEUTILISEES(
ID_ARME_PRINCIPALE 	INTEGER,
ID_ARME_SECONDAIRE 	INTEGER,
ID_USER 			INTEGER PRIMARY KEY,

CONSTRAINT fk_armes_princ FOREIGN KEY(ID_ARME_PRINCIPALE) REFERENCES ARMES(ID_ARME),
CONSTRAINT fk_armes_second FOREIGN KEY(ID_ARME_SECONDAIRE) REFERENCES ARMES(ID_ARME),
CONSTRAINT fk_armes_util FOREIGN KEY(ID_USER) REFERENCES utilisateur(ID)


);