#-*-coding: utf-8-*-
import dependencies
from Internal.DAO.DAOBalance import *
from Internal.DAO.DAOCSV import *
import Tkinter

daoCSV = DAOCSV()
daoBAL = DAO_Balance()

def fctSelect():
	daoCSV.read()

def fctCreer():
	daoBAL.ecrireDansDB(daoCSV.read())

root = Tkinter.Tk()
frame = Tkinter.Frame(root)
bSelect = Tkinter.Button(frame, text=u"Creer CSV à partir de DB",command=fctSelect).grid()
bCreate = Tkinter.Button(frame, text=u"Updater DB via CSV",command=fctCreer).grid()
frame.grid()
root.mainloop()