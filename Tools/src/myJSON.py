import sys
import dependencies
import json
from Internal.DAO.DAOMap import DAO_Map
from Internal.DAO.DAOEquipement import DAOEquipement
from Internal.DAO.DAOUtilisateur import DAOUtilisateur



class MyJSON:
   def lire(self,donnee):
      print donnee
      json_data = json.loads(donnee)
      DAOUtilisateur().ecrireDansDBDeSite(json_data)

   def ecrire(self, username):
      DTOMap = DAO_Map().lireDB()
      DTOEquipement = DAOEquipement().lireDB()
      DTOUtilisteur = DAOUtilisateur().connecter(username)
      info = {}
      info["success"]=0
      if DTOUtilisteur != None:
         info = DTOUtilisteur.getTousLesUtilisateurs()[0]
         info["success"]=1
      with open('..\Tools\src\data_DTOUtilisateur.json','w') as data_file:
         data = json.dumps(info)
         data_file.write(data)

      return json.dumps(info);