# -*- coding: utf-8 -*-
import dependencies
from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from direct.directbase import DirectStart
from direct.gui.DirectGui import *
from panda3d.core import *
#Module de Panda3D
from direct.showbase.ShowBase import ShowBase

#Modules internes
from Internal.DTO.dtoMap import DTOMap
 
#b1 = DirectButton(text = ("Button1", "click!", "roll", "disabled"),
#                  text_scale=0.1, borderWidth = (0.01, 0.01),
#                  relief=2)
 
#b2 = DirectButton(text = ("Button2", "click!", "roll", "disabled"),
 #                 text_scale=0.1, borderWidth = (0.01, 0.01),
  #                relief=2)
 
#l1 = DirectLabel(text = "Test1", text_scale=0.1)
#l2 = DirectLabel(text = "Test2", text_scale=0.1)
#l3 = DirectLabel(text = "Test3", text_scale=0.1)

class InterfaceMenuNiveau(ShowBase):
    def __init__(self):
        self.liste_de_dict_map = []
        self.dto= DTOMap()
        self.liste_de_dict_map=self.dto.getTousLesMaps()
        self.background=OnscreenImage(parent=render2d, image="Images/menubg.jpg")
        #pour test
        self.dict_map_ex = {}
        self.dict_map_ex["nom"] = "niveautest"
        self.dict_map_ex["nbCol"] = str(6)
        self.dict_map_ex["nbRow"] = str(6)
        self.dict_map_ex["tempsMin"] = str(40)
        self.dict_map_ex["tempsMax"] = str(50)
        self.dict_map_ex["etat"] = u"privé"
        self.dict_map_ex["joueu1"] = "roger"
        self.dict_map_ex["joueu2"] = "raymond"
        self.dict_map_ex["id"] = None

        def setText():
                bk_text = "Button Clicked"
                textObject.setText(self.bk_text)


        self.bk_titre = "Faites votre choix de niveau:"
        self.textObject = OnscreenText(text = self.bk_titre,
                                    pos = (0.1,0.7), #Position
                                    scale = 0.07, #scale
                                    fg=(1,1,1,1), #couleur du texte
                                    align=TextNode.ACenter, #alignement
                                    mayChange=0) #est ce que le texte peut changer?

        self.bk_text = "Version 0.0.0.0.0.1"
        self.textObject = OnscreenText(text = self.bk_text,
                                    pos = (-0.95,-0.95), #Position
                                    scale = 0.07, #scale
                                    fg=(1,0.5,0.5,1), #couleur du texte
                                    align=TextNode.ACenter, #alignement
                                    mayChange=1) #est ce que le texte peut changer?
        self.b = DirectButton(text = (u"Niveau aléatoire", "click!", "yolo", "disabled"),
                        scale=.1,
                        command=setText)
        self.b.setPos(0.1,-0.5,-0.5)

        self.b1 = DirectButton(text = ("Editeur de niveau", "click!", "yolo", "disabled"),
                        scale=.1,
                        command=setText)
        self.b1.setPos(0.1,-0.5,-0.7)

       
        numItemsVisible = 4
        itemHeight = 0.11

        self.myScrolledList = DirectScrolledList(
            decButton_pos= (0.35, 0, 0.53),
            decButton_text = "Dec",
            decButton_text_scale = 0.04,
            decButton_borderWidth = (0.005, 0.005),#Btn pour derouler liste
         
            incButton_pos= (0.35, 0, -0.02),
            incButton_text = "Inc",
            incButton_text_scale = 0.04,
            incButton_borderWidth = (0.005, 0.005),
         
            frameSize = (0.0, 0.7, -0.05, 0.59),
            frameColor = (1,1,1,0.5),
            pos = (-0.25, 0, 0),
            #items = [b1, b2], #liste item par defaut
            numItemsVisible = numItemsVisible,
            forceHeight = itemHeight, #item trop petit aura une hauteur mini
            itemFrame_frameSize = (-0.2, 0.2, -0.37, 0.11),
            itemFrame_pos = (0.35, 0, 0.4),
            )

        #myScrolledList.addItem(l1)
        #myScrolledList.addItem(l2)
        #myScrolledList.addItem(l3)


        for niveau in ['niveau1', 'niveau2', 'niveau3', 'niveau4','niveau5', 'niveau6', 'niveau7', 'niveau8']:
            l = DirectButton(text = (niveau, "click!", "roll", "disabled"),
                          text_scale=0.1, borderWidth = (0.01, 0.01),
                          relief=0)
            self.myScrolledList.addItem(l)
