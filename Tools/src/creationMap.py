#-*- coding: utf-8 -*-
import dependencies
from Tkinter import *
from decimal import Decimal
from Internal.connexionOracle.connexion_DB import Connexion
from Internal.DTO.dtoMap import DTOMap
from Internal.DAO.DAOMap import DAO_Map
from Internal.DAO.DAOUtilisateur import DAOUtilisateur
from Internal.DTO.DTOUtilisateur import DTOUtilisateur

nbPiece = 4

class Usager:
	def __init__(self,nom,id):
		self.nom = nom
		self.id = id
		
class SeConnexion:
	def __init__(self,root):
		self.root = root
		self.top = Toplevel(root)
		self.top.title("Connexion")
		screen_width = root.winfo_screenwidth()
		screen_height = root.winfo_screenheight()
		self.top.overrideredirect(1)
		self.top.geometry("300x200+%d+%d" % (screen_width/2-200,screen_height/2-200))
		self.strNom = StringVar()
		self.strPW = StringVar()
		self.strError = StringVar()
		self.valideInfo = False
		self.affichage();
		self.top.grab_set()
		self.quitter
		self.top.config(background="#15a708")
		root.lift()

	def setQuitter(self,quitter):
		self.quitter = quitter

	def quitter(self):
		self.quitter()
	
	def connect(self):
		#Ajouter getUtilisateur pour correction phase 3
		rep = self.validerPWUsager(self.strNom.get(),self.strPW.get())
		if(rep):
			self.usagerConnect = Usager(self.strNom.get(),rep.getUtilisateur()["ID"])#Enlever le [0] apres rep aussi
			self.root.title("Creation de map - Session de %s" % self.usagerConnect.nom)
			self.top.destroy()
		else:
			self.strError.set("Mauvaise information!")

	def validerPWUsager(self,identifiant,mdp):
		DTO = DAOUtilisateur().testerConnexion(identifiant,mdp)
		DTOMap().resetDTO()
		return DTO
	def affichage(self):
		Label(self.top,text="Identifiant").grid(row=0,column=0,padx=25,pady=25)
		Label(self.top,text="Mot de passe").grid(row=1,column=0)
		Entry(self.top,textvariable=self.strNom).grid(row=0,column=1)
		Entry(self.top,textvariable=self.strPW,show="*").grid(row=1,column=1)
		Button(self.top,text="Se connecter",command=self.connect,anchor=CENTER).grid(row=2,column=0,pady=10)
		Button(self.top,text="Quitter",command=self.quitter,anchor=CENTER,bg="red",fg="white").grid(row=3,column=0,pady=10)
		Label(self.top,textvariable=self.strError,fg="red",bg="#15a708").grid(row=2,column=1,pady=20)


class CreationMap:
	def __init__(self):
		self.listeEtats = [u"public",u"private",u"multi-joueur",u"disabled"]
		self.indiceEtat=0
		self.nbRow=9
		self.nbCol=9
		self.tempsMin = 0.5
		self.tempsMax = 2.0
		self.mouseX=0
		self.mouseY=0
		self.player1Position = (2,4)
		self.player2Position = (8,5)
		Connexion().meConnecter()
		self.root=Tk()
		screen_width = self.root.winfo_screenwidth()
		screen_height = self.root.winfo_screenheight()
		self.root.geometry("%dx%d+0+0" % (screen_width,screen_height-10))
		self.photoPlayer1 = PhotoImage(file="src/images/player1.gif")
		self.photoPlayer2 = PhotoImage(file="src/images/player2.gif")
		self.mainFrame=Frame(self.root)
		self.frameSetting=Frame(self.mainFrame,pady=100,padx=25)
		self.frameGrille=Frame(self.mainFrame)
		self.frameInfo=Frame(self.mainFrame,padx=25)
		self.frameInfo.grid(row=2,column=0,sticky="w")
		self.frameSetting.grid(row=0,column=0,sticky="n")
		self.frameGrille.grid(row=0,column=1)
		self.mainFrame.grid()
		self.grilleAssoc=self.creerGrilleInitiale()
		self.affichageInitialSetting()
		self.afficherGrille()
		self.root.bind('<Left>', self.leftKey)
		self.root.bind('<Right>', self.rightKey)
		self.root.bind('<Up>', self.upKey)
		self.root.bind('<Down>', self.downKey)
		self.root.bind('<s>', self.upState)
		self.root.bind('<a>', self.downState)
		self.root.bind('<z>', self.asssignerP1)
		self.root.bind('<x>', self.asssignerP2)
		self.root.bind('<w>', self.changerEtat)
		self.root.protocol("WM_DELETE_WINDOW", self.on_closing)
		self.root.after(100, self.verifierNbCaracNom)
		self.connection = SeConnexion(self.root)
		self.connection.setQuitter(self.on_closing)
		self.root.mainloop()

	def changerEtat(self,event):
		self.indiceEtat=(self.indiceEtat+1)%len(self.listeEtats)
		self.statStr.set(self.listeEtats[self.indiceEtat])
	
	def on_closing(self):
		Connexion().fermerConnexion()
		self.root.destroy()

	def leftKey(self,event):
		self.mouseY=(self.mouseY-1)%self.nbRow
		self.afficherGrille()

	def rightKey(self,event):
		self.mouseY=(self.mouseY+1)%self.nbCol
		self.afficherGrille()

	def upKey(self,event):
		self.mouseX=(self.mouseX-1)%self.nbCol
		self.afficherGrille()

	def downKey(self,event):
		self.mouseX=(self.mouseX+1)%self.nbCol
		self.afficherGrille()

	def upState(self,event):
		case = self.grilleAssoc[self.mouseX][self.mouseY]
		case=(case+1)%nbPiece
		self.grilleAssoc[self.mouseX][self.mouseY] = case
		self.afficherGrille()

	def downState(self,event):
		case=self.grilleAssoc[self.mouseX][self.mouseY]
		self.grilleAssoc[self.mouseX][self.mouseY]=(case-1)%nbPiece
		self.afficherGrille()
		
	def asssignerP2(self,event):
		if (self.mouseX,self.mouseY) != self.player1Position:
			self.player2Position = (self.mouseX,self.mouseY)
			self.afficherGrille()
			
	def asssignerP1(self,event):
		if (self.mouseX,self.mouseY) != self.player2Position:
			self.player1Position = (self.mouseX,self.mouseY)
			self.afficherGrille()
	
	def creerGrilleInitiale(self):
		grille = []
		for i in range(0,12):
			grille.append([0,0,0,0,0,0,0,0,0,0,0,0])
		return grille

	def affichageInitialSetting(self):
		grille = self.grilleAssoc
		up = "up"
		down = "down"
		yStr = StringVar()
		yStr.set("9")
		xStr = StringVar()
		xStr.set("9")
		minStr = StringVar()
		minStr.set(str(self.tempsMin))
		maxStr = StringVar()
		maxStr.set(str(self.tempsMax))
		self.statStr = StringVar()
		self.statStr.set("Public")

		def grossirY():
			if(self.nbCol<12):
				self.nbCol+=1
				yStr.set(self.nbCol)
				self.afficherGrille()
		def reduireY():
			if(self.nbCol>6):
				self.nbCol-=1
				yStr.set(self.nbCol)
				self.afficherGrille()
		def grossirX():
			if(self.nbRow<12):
				self.nbRow+=1
				xStr.set(self.nbRow)
				self.afficherGrille()
		def reduireX():
			if(self.nbRow>6):
				self.nbRow-=1
				xStr.set(self.nbRow)
				self.afficherGrille()
		def grossirMin():
			if(self.tempsMin<self.tempsMax):
				self.tempsMin=self.tempsMin + 0.1
				minStr.set(Decimal(str(round(self.tempsMin,1))))
		def reduireMin():
			if(self.tempsMin>0):
				self.tempsMin=self.tempsMin-0.1
				minStr.set(Decimal(str(round(self.tempsMin,1))))
		def grossirMax():
			self.tempsMax=self.tempsMax+0.1
			maxStr.set(Decimal(str(round(self.tempsMax,1))))
		def reduireMax():
			if(self.tempsMax>self.tempsMin):
				self.tempsMax=self.tempsMax-0.1
				maxStr.set(Decimal(str(round(self.tempsMax,1))))
		def enregistrer():
			dao = DAO_Map()
			dao.ecrireDansDB(self.enregistrerMap())
			self.nomMap.delete(0, 'end')
			self.grilleAssoc=self.creerGrilleInitiale()
			self.afficherGrille()
		def modifierTitre():
			if self.nomMap.cget("state")=='disabled':
				self.nomMap.configure(state='normal')
			else:
				self.nomMap.configure(state='disabled')

		Button(self.frameSetting,text=up,command=grossirY)			.grid(row=0,column=2)
		Label(self.frameSetting,textvariable=yStr)					.grid(row=0,column=1)
		Button(self.frameSetting,text=down,command=reduireY)		.grid(row=0,column=0)

		Button(self.frameSetting,text=up,command=grossirX)			.grid(row=1,column=2)
		Label(self.frameSetting,textvariable=xStr)					.grid(row=1,column=1)
		Button(self.frameSetting,text=down,command=reduireX)		.grid(row=1,column=0)

		Button(self.frameSetting,text=up,command=grossirMin)		.grid(row=2,column=2)
		Label(self.frameSetting,textvariable=minStr)				.grid(row=2,column=1)
		Button(self.frameSetting,text=down,command=reduireMin)		.grid(row=2,column=0)

		Button(self.frameSetting,text=up,command=grossirMax)		.grid(row=3,column=2)
		Label(self.frameSetting,textvariable=maxStr)				.grid(row=3,column=1)
		Button(self.frameSetting,text=down,command=reduireMax)		.grid(row=3,column=0)

		Label(self.frameSetting,text=u"État")						.grid(row=4,column=0)
		Label(self.frameSetting,textvariable=self.statStr)			.grid(row=4,column=1)
		Label(self.frameSetting,text="Titre")						.grid(row=5,column=0)
		Label(self.frameSetting,text=u"(Max 10 caractères)")		.grid(row=7,column=1)
		self.nomMap = Entry(self.frameSetting)
		self.nomMap.grid(row=5,column=1,rowspan=2)

		Button(self.frameSetting,text="Enregistrer map",command=enregistrer).grid(row=8,column=1,rowspan=2)
		Button(self.frameSetting,text="",command=modifierTitre).grid(row=5,column=2)
		Label(self.frameInfo,text=u"a -> Case précédente")	.grid()
		Label(self.frameInfo,text=u"s -> Case suivante")	.grid()
		Label(self.frameInfo,text=u"z -> Placer joueur 1")	.grid()
		Label(self.frameInfo,text=u"x -> Placer joueur 2")	.grid()
		Label(self.frameInfo,text=u"w -> Changer etat")		.grid()
		
	def afficherGrille(self):
		for child in self.frameGrille.winfo_children():
			child.destroy()
		for i in range(0,self.nbRow):
			for j in range(0,self.nbCol):
				valCase = self.grilleAssoc[i][j]
				if valCase==0:
					textcase="plancher"
					color="orange"
				elif valCase==1:
					textcase="mur"
					color="green"
				elif valCase==2:
					textcase="mur mobile haut"
					color="magenta"
				else:
					textcase="mur mobile bas"
					color="cyan"
				unLabel=Label(self.frameGrille,text=str(textcase),width=15,height=4,background=color)
				if self.mouseX==i and self.mouseY==j:
					unLabel.config(background="blue",fg="white")
				unLabel.grid(row=i,column=j,padx=3,pady=3)
				if (i,j) == self.player1Position:
					Label(self.frameGrille,image=self.photoPlayer1).grid(row=i,column=j)
				if (i,j) == self.player2Position:
					Label(self.frameGrille,image=self.photoPlayer2).grid(row=i,column=j)
		self.frameGrille.grid(row=0,column=1)
		
	def enregistrerMap(self):
		dict_map ={}
		dict_map["nom"] = self.nomMap.get()
		dict_map["nbCol"] = self.nbCol
		dict_map["nbRow"] = self.nbRow
		dict_map["tempsMin"] = self.tempsMin
		dict_map["tempsMax"] = self.tempsMax
		dict_map["etat"] = self.statStr.get()
		dict_map["joueur1"] = self.player1Position
		dict_map["joueur2"] = self.player2Position
		dict_map["createur"] = self.connection.usagerConnect.id
		dict_map["id"] = None
		grille = []
		ligne = []

		for j in range(0,self.nbCol):
			ligne =[]
			for i in range(0,self.nbRow):
				ligne.append(self.grilleAssoc[j][i])
			grille.append(ligne)

		dict_map["map"] = grille
		dto = DTOMap()
		dto.ajouterMap(dict_map)
		return dto

	def verifierNbCaracNom(self):
		if len(self.nomMap.get()) > 9:
			text = self.nomMap.get()[0:9]
			self.nomMap.delete(0, 'end')
			self.nomMap.insert(INSERT,text)
		self.root.after(100, self.verifierNbCaracNom)
	

if __name__ == '__main__':
	CreationMap()

		

