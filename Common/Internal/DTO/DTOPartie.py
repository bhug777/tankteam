from ..DAO.DAOPartie import DAOPartie


##Exceptionnellement le DTO va devoir connaitre le DAO puisque c'est la DB qui connait le ID de la partie
class DTOPartie:
	def __init__(self):
		self.idPartie = None
		self.gagant = None
		self.creerPartieDansDB()

	def creerPartieDansDB(self):
		self.idPartie = DAOPartie().creerPartie()
