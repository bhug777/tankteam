
class DTOEquipement():
	dict_partage = {}
	def __init__(self):
		self.__dict__ = self.dict_partage
		if not hasattr(self,"liste_armes"):
			self.liste_armes = []
	
	def ajouterArme(self,dictionnaire):
		for arme in self.liste_armes:
			if arme["NOM"] == dictionnaire["NOM"]:
				return
		self.liste_armes.append(dictionnaire)

	def getTousLesArmes(self):
		return self.liste_armes

	def getDictArmeUtilisateur(self,liste_keys):
		dictArmes = {}
		for arme in self.liste_armes:
			for item in liste_keys: 
				if arme["ID"] == item[0]:
					dictArmes[arme["NOM"]] = {}
					dictArmes[arme["NOM"]]["ARME"] = arme
					dictArmes[arme["NOM"]]["QTE"] = item[1]
		return dictArmes