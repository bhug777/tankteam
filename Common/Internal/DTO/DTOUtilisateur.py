class DTOUtilisateur():
	def __init__(self):
		self.liste_utilisateurs = []
	
	def ajouterUtilisateur(self,tableau):
		self.liste_utilisateurs.append(tableau)

	def getTousLesUtilisateurs(self):
		return self.liste_utilisateurs

	def getUtilisateur(self):
		return self.liste_utilisateurs[0]

	def getUtilisateurParID(self,idUtil):
		for util in self.liste_utilisateurs:
			if util["ID"]==idUtil:
				return util

