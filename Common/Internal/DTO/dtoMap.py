
class DTOMap():
	dict_partage = {}
	def __init__(self):
		self.__dict__ = self.dict_partage
		if not hasattr(self,"liste_de_dict_map"):
			self.liste_de_dict_map = []
	
	def ajouterMap(self,dict):
		for carte in self.liste_de_dict_map:
			if carte["nom"] == dict["nom"]:
				return
		self.liste_de_dict_map.append(dict)

	def getTousLesMaps(self):
		return self.liste_de_dict_map

	def getMapParID(self,idMap):
		for carte in self.liste_de_dict_map:
			if carte["id"]==idMap:
				return carte

	def getMapsDunUtilisateur(self,listeIdMap):
		dictCartes = {}
		for carte in self.liste_de_dict_map:
			if carte["id"] in listeIdMap:
				dictCartes[carte["nom"]] = carte
		return dictCartes

	def getMapsDunJoueur(self, nom_joueur):
		liste_map = []
		for carte in self.liste_de_dict_map:
			if self.liste_de_dict_map[carte]["proprietaire"] == nom_joueur:
				liste_map.append(self.liste_de_dict_map[carte])

		return liste_map

	def getMapParEtat(self,etat,liste_joueurs_exclus):
		liste_map = []
		for carte in self.liste_de_dict_map:
			if self.liste_de_dict_map[carte]["etat"] == etat:
				if self.liste_de_dict_map[carte]["proprietaire"] not in liste_joueurs_exclus:
					liste_map.append(self.liste_de_dict_map[carte])

		return liste_map

	def resetDTO(self):
		self.liste_de_dict_map = []


