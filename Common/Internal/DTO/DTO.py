
class DTO:
	def __init__(self):
		self.dict_params = {}
		
	def getAllValues(self):
		return self.dict_params

	def set(self, dictionnaire):
		self.dict_params = dictionnaire

	def getItem(self,key):
		return self.dict_params[key]

	def getParamItem(self, key, param):
		return self.dict_params[key][param]

	def getValParamItem(self,key,param):
		return self.dict_params[key][param][2]