from copy import deepcopy

class Case():
	def __init__(self,posX,posY):
		self.posX = posX
		self.posY = posY
		self.tempsPasser = 0
		self.dommageRecu = 0
		self.dommageDonne = 0

	def augmenterTemps(self,temps):
		self.tempsPasser += temps

	def augmenterDomRecu(self,dommage):
		#print "dmg recu " + str(dommage)
		self.dommageRecu += dommage

	def augmanteDomDonne(self,dommage):
		#print "dmg donne " + str(dommage)
		self.dommageDonne += dommage

	def getInfoCase(self):
		return [self.posX,self.posY,self.tempsPasser,self.dommageRecu,self.dommageDonne]


class Heatmap():
	def __init__(self,idPartie,nbX,nbY,idMap):
		self.idMap = idMap
		self.nbX = nbX
		self.nbY = nbY
		self.idPartie = idPartie
		self.joueur1 = JoueurHeatmap(nbX,nbY)
		self.joueur2 = JoueurHeatmap(nbX,nbY)

	def getInfoPourDAO(self):
		return self

#Creer class dans tankem
class JoueurHeatmap():
	def __init__(self,nbX,nbY): #On passe la taille de la map
		self.idJoueur = None
		self.carte = Carte(nbX,nbY)

	def getUneCase(self,posX,posY):
		return self.carte.getUneCase(posX,posY)

	def augmenterTemps(self,posX,posY,temps):
		self.getUneCase(posX,posY).augmenterTemps(temps)

	def augmenterDommageRecu(self,posX,posY,dommage):
		self.getUneCase(posX,posY).augmenterDomRecu(dommage)

	def augmenterDommageDonne(self,posX,posY,dommage):
		self.getUneCase(posX,posY).augmanteDomDonne(dommage)

	def getInfoPourDAO(self):
		return self.carte.getInfoCarte()

	def setIdJoueur(self,idJoueur):
		self.idJoueur = idJoueur


class Carte():
	def __init__(self,nbX,nbY):
		self.nbX = nbX
		self.nbY = nbY
		self.cases = []
		self.initialiserCases()

	def initialiserCases(self):
		for y in range(0,self.nbY):
			uneLigne = []
			for x in range(0,self.nbX):
				uneLigne.append(Case(x,y))
			cLigne = uneLigne[:]
			self.cases.append(cLigne)
		#print str(self.cases)

	def getUneCase(self,x,y):
		#print str(self.cases[x][y])
		return self.cases[x][y]

	def getInfoCarte(self):
		info = []
		for rangee in range(0,self.nbX):
			for j in range(0,self.nbY):
				info.append(self.getUneCase(j,rangee).getInfoCase())
		return info
