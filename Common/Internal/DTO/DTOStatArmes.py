
liste_arme = ["Canon","Grenade","Mitraillette","Piege","Shotgun","Guide"]

class DTOStatArmes:
	def __init__(self):
		self.idJoueur = None
		self.armes = {}
		self.instancierArmes()

	def instancierArmes(self):
		for arme in liste_arme:
			self.armes[arme] = Arme(arme)

	def getArme(self, nom):
		return self.armes[nom]

	def augmenterDegat(self,nom,valeur):
		self.getArme(nom).augmenterDegat(valeur)

	def plusUnTire(self,nom):
		self.getArme(nom).plusUnTire()

	def setIdJoueur(self,idj):
		self.idJoueur = idj

class Arme:
	def __init__(self,nomArme):
		self.nomArme = nomArme
		self.degat = 0
		self.nbFoisTire = 0

	def augmenterDegat(self,valeur):
		self.degat += valeur

	def plusUnTire(self):
		self.nbFoisTire += 1


