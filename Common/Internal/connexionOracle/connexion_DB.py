import cx_Oracle

class Connexion:
	dict_partage = {}
	def __init__(self):
		self.__dict__ = self.dict_partage
		if not hasattr(self,"ma_conn"):
			self.ma_conn = None

	def meConnecter(self):

		#le code devrait etre comme suit:
		#if self.ma_conn==None:
		#try:
		#	self.ma_conn = cx_Oracle.connect("e0442162","adsi72cq72", "10.57.4.60/DECINFO.edu")
		#except cx_Oracle.DatabaseError as er:
		#	self.ma_conn=None
		#	error, = er.args
		#	print "Erreur de connexion"
		#	print error.code #pour faire recherche sur google
		#	print error.message #Donne une description de l'erreur
		#	print error.context #Donne la ligne (et caractere) de l'erreur

		#return self.ma_conn
		#Mais nous eprouvons des problemes de connexion alors nous avons
		#decider de creer une nouvelle connection a chaque appel
		try:
			self.ma_conn = cx_Oracle.connect("e0442162","adsi72cq72", "10.57.4.60/DECINFO.edu")
		except cx_Oracle.DatabaseError as er:
			self.ma_conn=None
			error, = er.args
			print "Erreur de connexion"
			print error.code #pour faire recherche sur google
			print error.message #Donne une description de l'erreur
			print error.context #Donne la ligne (et caractere) de l'erreur

		return self.ma_conn

	def executerSelectListe(self,ex,liste):
		self.meConnecter()
		if self.ma_conn!=None:
			try:
				#print str(self.ma_conn)
				cur = self.ma_conn.cursor()
				cur.execute(ex,liste)
				donnees = cur.fetchall()
				self.ma_conn.commit()
			except cx_Oracle.DatabaseError as er:
				self.ma_conn=None
				error, = er.args
				print "Erreur de connexion"
				print error.code #pour faire recherche sur google
				print error.message #Donne une description de l'erreur
				print error.context #Donne la ligne (et caractere) de l'erreur

		return donnees

	def executemanySelect(self,ex,liste):
		self.meConnecter()
		if self.ma_conn!=None:
			try:
				cur = self.ma_conn.cursor()
				cur.executemany(ex,liste)
				donnees = cur.fetchall()
				self.ma_conn.commit()
			except cx_Oracle.DatabaseError as er:
				self.ma_conn=None
				error, = er.args
				print "Erreur de connexion"
				print error.code #pour faire recherche sur google
				print error.message #Donne une description de l'erreur
				print error.context #Donne la ligne (et caractere) de l'erreur

		return donnees

	def executemanyInsert(self,ex,liste):
		self.meConnecter()
		if self.ma_conn!=None:
			try:
				cur = self.ma_conn.cursor()
				cur.executemany(ex,liste)
				self.ma_conn.commit()
			except cx_Oracle.DatabaseError as er:
				self.ma_conn=None
				error, = er.args
				print "Erreur de connexion"
				print str(error)


	def executerSelect(self,ex):
		self.meConnecter()
		if self.ma_conn!=None:
			try:
				cur = self.ma_conn.cursor()
				cur.execute(ex)
				donnees = cur.fetchall()
				self.ma_conn.commit()
			except cx_Oracle.DatabaseError as er:
				self.ma_conn=None
				error, = er.args
				print "Erreur de connexion"


		return donnees

	def executerInsertListe(self,ex,liste):
		self.meConnecter()
		if self.ma_conn!=None:
			try:
				cur = self.ma_conn.cursor()
				cur.execute(ex,liste)
				self.ma_conn.commit()
			except cx_Oracle.DatabaseError as er:
				self.ma_conn=None
				error, = er.args
				print "Erreur de connexion"
			



	def fermerConnexion(self):
		try:
			self.ma_conn=None;
		except cx_Oracle.DatabaseError as er:
			error, = er.args
			print "Erreur loprs de la fermeture de la connexion"
			print error.code #pour faire recherche sur google
			print error.message #Donne une description de l'erreur
			print error.context #Donne la ligne (et caractere) de l'erreur


