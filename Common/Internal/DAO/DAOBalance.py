#-*-coding: utf-8-*-
from ..connexionOracle.connexion_DB import Connexion
import cx_Oracle
from ..DTO.DTO import *

class DAO_Balance():	
	def lireDB(self):
		dict_params ={}
		ma_connexion = Connexion().meConnecter()
		#print str(ma_connexion)
		if ma_connexion != None:
			try:
				cur = ma_connexion.cursor()
				#On recupere tout les elements de la table item_tank
				cur.execute("SELECT * FROM caracteristique_item_tankem")
				resultCurItem = cur.fetchall()
				cur.close()
				cur = ma_connexion.cursor()
				cur.execute("SELECT * FROM message_tankem")
				resultCurMessage = cur.fetchall()
				for r in resultCurItem:
					id = r[0]
					key = id[:-3]
					keyParam = id[-3:]
					if key not in dict_params:
						dict_params[key]={}
					dict_params[key][keyParam]=[r[2],r[1],r[4],r[3]]
				for r in resultCurMessage:
					dict_params["message_accueil"] = r[0]
					dict_params["message_debut"] = r[1]
					dict_params["message_fin"] = r[2]
			except cx_Oracle.DatabaseError as er:
				error, = er.args
				print error
				print (u"Erreur de connexion")
				print (error.code) #pour faire recherche sur google
				print (error.message) #Donne une description de l'erreur
				print (error.context) #Donne la ligne (et caractere) de l'erreur
			cur.close()
			ma_connexion.close()
			dto = DTO()
			dto.set(dict_params)
			#print str(dto.getAllValues())
			return dto

	def ecrireDansDB(self,dto):
		ma_connexion = Connexion().meConnecter()
		#print str(ma_connexion)
		dictDonnee = dto.getAllValues()
		dict_key_longueur_message = {"message_fin":70,"message_debut":50,"message_accueil":60}
		liste_donnee_pour_db = []
		liste_donnee_message = []
		liste_erreur = []
		if ma_connexion != None:
			try:
				cur = ma_connexion.cursor()
				for key in dictDonnee:
					if key not in dict_key_longueur_message:
						for keyParam in dictDonnee[key]:
							params = dictDonnee[key][keyParam] 
							if float(params[2]) > float(params[1]) or float(params[2]) < float(params[0]):
								liste_erreur.append(key+keyParam)
							else:
								liste_donnee_pour_db.append((params[0],params[1],params[2],key+keyParam))
					else:
						if len(dictDonnee[key]) > dict_key_longueur_message[key]:
							liste_erreur.append(dictDonnee[key])
						else:
							liste_donnee_message.append((key,dictDonnee[key],1))
				cur.executemany("""UPDATE caracteristique_item_tankem 	
				SET val_min=:1,val_max=:2, valeur=:3 
				WHERE id=:4""",liste_donnee_pour_db)	
				cur.execute("UPDATE message_tankem SET message_accueil=\'" + dictDonnee["message_fin"] + "\' WHERE id_mess=1""")	
				cur.close()
				cur = ma_connexion.cursor()
				cur.execute("UPDATE message_tankem SET message_fin=\'" +  dictDonnee["message_fin"] + "\' WHERE id_mess=1")	
				cur.close()
				cur = ma_connexion.cursor()
				cur.execute("UPDATE message_tankem SET message_accueil=\'" +  dictDonnee["message_accueil"] + "\' WHERE id_mess=1")	
				for erreur in liste_erreur:
					print u"le champ " + erreur + u" n'a pas été updater puisqu'il ne répond pas aux critères"
			except cx_Oracle.DatabaseError as er:
				error, = er.args
				print error
				print (u"Erreur de connexion")
				print (error.code) #pour faire recherche sur google
				print (error.message) #Donne une description de l'erreur
				print (error.context) #Donne la ligne (et caractere) de l'erreur
			ma_connexion.commit()
			cur.close()
			ma_connexion.close()