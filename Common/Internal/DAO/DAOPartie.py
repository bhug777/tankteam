from ..connexionOracle.connexion_DB import Connexion
import cx_Oracle


class DAOPartie:
	def creerPartie(self,idcarte):
		ma_connexion = Connexion()
		#Instancier une partie
		insert = "INSERT INTO PARTIE(ID_NIVEAU) VALUES(:1)"
		ma_connexion.executerInsertListe(insert,{":1":idcarte})
		idp = ma_connexion.executerSelect("SELECT max(id) FROM PARTIE")
		return idp[0][0]

	def updaterPartie(self,id1,id2,idgagnant,idp):
		ma_connexion = Connexion()
		insert = "UPDATE PARTIE SET ID_J1=:1,ID_J2=:2,ID_GAGNANT=:3 WHERE ID= :4"
		ma_connexion.executerInsertListe(insert,{":1":id1,":2":id2,":3":idgagnant,":4":idp})

