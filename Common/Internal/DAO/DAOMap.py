#-*-coding: utf-8-*-
from ..connexionOracle.connexion_DB import Connexion
import cx_Oracle
from ..DTO.dtoMap import DTOMap


class DAO_Map():	
	def ecrireDansDB(self,dto):
		dict = dto.getTousLesMaps()
		#print str(dict)
		ma_connexion = Connexion()
		insertCreateurNiveau = u"INSERT INTO NIVEAUXCREATION(ID_USER,ID_NIVEAU)VALUES(:1,:2)"
		requeteInsertMaptt = u"INSERT INTO mapTT(nbX,nbY,etat,nom,tempsMin,tempsMax)VALUES(:1,:2,:3,:4,:5,:6)"
		for map in dict:
			ma_connexion.executerInsertListe(requeteInsertMaptt,{":1":map["nbRow"],":2":map["nbCol"],":3":map["etat"],":4":map["nom"],":5":map["tempsMin"],":6":map["tempsMax"]})
			donnee = ma_connexion.executerSelect("SELECT max(id) FROM mapTT")
			id = donnee[0][0]
			sqlRequete = u"INSERT INTO valeurCase(id_mapTT,posx,posy,valeur,id)VALUES(:1,:2,:3,:4,:5)"
			sqlRequete2 = u"INSERT INTO SPAWN(IDNIVEAU,posx,posy)VALUES(:1,:2,:3)"
			tab_val_requete = []
			indexeCol=0
			indexeRow=0
			indexId = 0
			cases = map["map"]
			for row in cases:
				for col in row:
					tab_val_requete.append((id,indexeCol,indexeRow,cases[indexeRow][indexeCol],indexId))
					indexeCol+=1
					indexId +=1
				#tenir compte de l'index des cases
				indexeRow+=1
				indexeCol=0
			tabValues = []
			tabValues.append((id,map["joueur1"][0],map["joueur1"][1]))
			tabValues.append((id,map["joueur2"][0],map["joueur2"][1]))
			ma_connexion.executemanyInsert(sqlRequete,tab_val_requete)
			ma_connexion.executemanyInsert(sqlRequete2,tabValues)
			ma_connexion.executerInsertListe(insertCreateurNiveau,(map["createur"],id))			

	def lireDB(self):
		ma_connexion = Connexion().meConnecter()
		if ma_connexion != None:
			try:
				cur = ma_connexion.cursor()
				cur.execute("SELECT * FROM mapTT")
				donnee = cur.fetchall()
				dto=DTOMap()
				for row in donnee:
					dictMap={}
					dictMap["id"]=row[0]
					dictMap["nbCol"]=row[1]
					dictMap["nbRow"]=row[2]
					dictMap["etat"]=row[3]
					dictMap["nom"]=row[4]
					dictMap["tempsMin"]=row[5]
					dictMap["tempsMax"]=row[6]
					#dictMap["nomProprio"]=row[7]
					mapTempo = list()
					uneLigne=list()
					for i in range(0,row[2]+1):uneLigne.append(i) 
					for i in range(0,row[1]+1):
						nouvLigne = uneLigne[:]
						mapTempo.append(nouvLigne)
					requeteSELECTvaleur = "SELECT posy,posx,valeur FROM valeurcase WHERE id_mapTT=:1" 
					cur.execute(requeteSELECTvaleur,{":1":row[0]})
					donnee = cur.fetchall()
					for case in donnee:
						mapTempo [case[1]][case[0]]=case[2]
					dictMap["map"]=mapTempo 
					dto.ajouterMap(dictMap)
					cur.close()
					#Code pour get les spawn a completer:
					cur = ma_connexion.cursor()
					requeteSpawn = "SELECT posx, posy FROM spawn WHERE idniveau=:1"
					cur.execute(requeteSpawn,{":1":row[0]})
					donnee = cur.fetchall()
					if len(donnee)>1:
						dictMap["joueur1"]=(donnee[0][0],donnee[0][1])
						dictMap["joueur2"]=(donnee[1][1],donnee[1][1])
						
			except cx_Oracle.DatabaseError as er:
				error, = er.args
				print error
				print (u"Erreur de connexion")
				#print (error.code) #pour faire recherche sur google
				#print (error.message) #Donne une description de l'erreur
				#print (error.context) #Donne la ligne (et caractere) de l'erreur
			ma_connexion.commit()
			cur.close()
			return dto