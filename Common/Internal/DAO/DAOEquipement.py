#-*-coding: utf-8-*-
from ..connexionOracle.connexion_DB import Connexion
import cx_Oracle
from ..DTO.DTOEquipement import DTOEquipement

class DAOEquipement():	

	def lireDB(self):
		ma_connexion = Connexion()
		dto =  DTOEquipement()
		requeteGetArme = "SELECT ID_ARME,NOM,DESCRIPTION FROM ARMES"
		donnees = ma_connexion.executerSelect(requeteGetArme)
		for ligne in donnees:
			idArme = ligne[0]
			uneArme = {}
			uneArme["ID"]=idArme
			uneArme["NOM"]=ligne[1]
			uneArme["DESCRIPTION"]=ligne[2] 
			dto.ajouterArme(uneArme)
		return dto

