#-*-coding: utf-8-*-
import csv
from ..DTO.DTO import *
from Tkinter import Tk
from tkFileDialog import askopenfilename

#empeche une fenetre tkinter d'ouvrir
Tk().withdraw()
class DAOCSV:
	def choisirFichierCSV(self):
		nomFichier = askopenfilename(filetypes=[("CSV Files","*.csv"),("All Files","*")],defaultextension="") # defaut pr etre safe
		ouvrirFichier= open(nomFichier)
		return nomFichier

	def createCSV(self,dto):
		DAOValues= dto.getAllValues()
		nomFichier=self.choisirFichierCSV()
		with open(nomFichier,"w") as fichier:
			writer = csv.writer(fichier, delimiter=";", quotechar='"', lineterminator='\n')
			writer.writerow([u"ID",u"Min", u"Max",u"Valeur",u"Defaut",u"Description"])
			for item in DAOValues:
				if item == "message_fin" or item == "message_accueil" or item == "message_debut":
						writer.writerow((item,DAOValues[item],"",""))					
				else:
					for param in DAOValues[item]:
						dicti = DAOValues[item][param]
						writer.writerow([item+param,dicti[0],dicti[1],dicti[2],dicti[3]])				

	def read(self):
		listValeurs={}
		listeMessage=["message_debut","message_accueil","message_fin"]
		nomFichier = self.choisirFichierCSV()
		with open(nomFichier,"r") as fichier:
			reader = csv.reader(fichier,delimiter=";", quotechar='"') #preciser le dialecte est important
			for row in reader:
				if row[0] == "ID":
					pass
				elif row[0] not in listeMessage:
					key = row[0][:-3]
					keyparam = row[0][-3:]
					if key not in listValeurs:
						listValeurs[key]={}
					listValeurs[key][keyparam] = [row[1],row[2],row[3]]
				else:
					listValeurs[row[0]] = row[1]

				#for item in row:
					#listValeurs.append(",".join(item))
		dto=DTO()
		dto.set(listValeurs)
		return dto
