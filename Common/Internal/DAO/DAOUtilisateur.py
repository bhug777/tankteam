#-*-coding: utf-8-*-
from ..connexionOracle.connexion_DB import Connexion
import cx_Oracle
from ..DTO.DTOUtilisateur import DTOUtilisateur
from ..DAO.DAOEquipement import DAOEquipement
from ..DTO.DTOEquipement import DTOEquipement
from ..DAO.DAOMap import DAO_Map
import bcrypt


class DAOUtilisateur():	


	def updateAjouterAuFavoris(self,idUser,idMap):
		ma_connexion = Connexion()
		verifierSiExiste = "SELECT * FROM NIVEAUXFAVORIS WHERE ID_USER=:iu AND ID_NIVEAU=:idn"
		ajouterAFav = "INSERT INTO NIVEAUXFAVORIS(ID_USER,ID_NIVEAU)VALUES(:iu,:idn)"
		fav = ma_connexion.executerSelectListe(verifierSiExiste,{":iu":int(idUser),":idn":int(idMap)})
		if(len(fav)<1):
			ma_connexion.executemanyInsert(ajouterAFav,[{":iu":int(idUser),":idn":int(idMap)}])

	def updateQteArmeFinPartie(self,liste_nouvelle_arme,liste_ancienne_arme):
		ma_connexion = Connexion()
		listeNouv =[]
		listeAncien = []
		updateQte = "UPDATE UTILISATEURS_ARMES SET QUANTITE=:1 WHERE ID_USER=:2 AND ID_ARMES=:3"
		ajoutArme = "INSERT INTO UTILISATEURS_ARMES(ID_USER,ID_ARMES,QUANTITE)VALUES(:1,:2,:3)"
		for item in liste_ancienne_arme:
			item[2]+=1
			listeAncien.append((item[2],item[1],item[0]))
		for item in liste_nouvelle_arme:
			listeNouv.append((item[1],item[0],1))
		if len(listeNouv)>0:
			ma_connexion.executemanyInsert(ajoutArme,listeNouv)
		if len(listeAncien)>0:
			ma_connexion.executemanyInsert(updateQte,listeAncien)

	def updateQteArme(self,liste_nomArme_idUser_qte):
		ma_connexion = Connexion()
		listeAUpdate =[]
		listeASupprimer = []
		updateQte = "UPDATE UTILISATEURS_ARMES SET QUANTITE=:1 WHERE ID_USER=:2 AND ID_ARMES=:3"
		deleteAreme = "DELETE FROM UTILISATEURS_ARMES WHERE ID_USER=:1 AND ID_ARMES=:2"
		for item in liste_nomArme_idUser_qte:
			item[2]-=1
			if item[2] == 0:
				listeASupprimer.append((item[1],item[0]))
			else:
				listeAUpdate.append((item[2],item[1],item[0]))
		if len(listeAUpdate)>0:
			ma_connexion.executemanyInsert(updateQte,listeAUpdate)
		if len(listeASupprimer)>0:
			ma_connexion.executemanyInsert(deleteAreme,listeASupprimer)

	def ecrireDansDBDeSite(self,dto):
		ma_connexion = Connexion()
		joueur = dto
		#print str(dto)
		mdp = joueur['MOT_DE_PASSE'].replace("!","$")
		creerUsager = "INSERT INTO UTILISATEUR(NOM_UTILISATEUR,MOT_DE_PASSE,NOM,PRENOM,QUESTION_A,QUESTION_B,REPONSE_A,REPONSE_B,COULEUR)VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9)"
		ma_connexion.executerInsertListe(creerUsager,(joueur['NOM_UTILISATEUR'],mdp,joueur['NOM'],joueur['PRENOM'],joueur['QUESTION_A'],joueur['QUESTION_B'],joueur['REPONSE_A'],joueur['REPONSE_B'],joueur['COULEUR']))

	def ecrireDansDB(self,dto):
		ma_connexion = Connexion()
		joueur = dto.getTousLesUtilisateurs()[0]
		idUser = joueur["ID"]
		deleteArme = "DELETE FROM UTILISATEURS_ARMES WHERE ID_USER=:1"
		updaterInfo = "INSERT INTO UTILISATEURS_ARMES(ID_USER,ID_ARMES,QTE)VALUES(:1,:2,:3)"
		deleteNiveauFav = "DELETE FROM NIVEAUXFAVORIS WHERE ID_USER=:1"
		updateNiveauFav = "INSERT INTO NIVEAUXFAVORIS(ID_USER,ID_NIVEAU)VALUES(:1,:2)"
		ma_connexion.executerInsertListe(deleteArme,([idUser]))
		ma_connexion.executerInsertListe(deleteNiveauFav,([idUser]))
		tab_armes_utilisees = [joueur["ARME_PRINCIPALE"],joueur["ARME_SECONDAIRE"]]
		dict_armes = joueur["EQUIPEMENT"]
		list_arme = []
		for key in dict_armes:
			list_arme.append(idUser,dict_armes[key]["ARME"]["ID"],dict_armes[key]["QTE"])
		ma_connexion.executemanyInsert(updaterInfo,list_arme)


		ma_connexion.executemanyInsert(updateNiveauFav,(idUser,))
		dict_map = joueur["MAPS_PREFEREES"]
		liste_map = []
		for key in dict_map:
			liste_map.append(idUser,dict_map[key]["id"])
		ma_connexion.executemanyInsert(updateNiveauFav,listeMap)

	def lireDB(self,DTOMap,DTOEquipement,username):
		ma_connexion = Connexion()
		dto =  DTOUtilisateur()
		#"SELECT ID,NOM_UTILISATEUR,NOM,PRENOM,QUESTION_A,QUESTION_B,REPONSE_A,REPONSE_B,DATE_CREATION,COULEUR FROM utilisateur WHERE NOM_UTILISATEUR=:nom"
		requeteGetUtilisateurs = "SELECT ID,NOM_UTILISATEUR,NOM,PRENOM,QUESTION_A,QUESTION_B,REPONSE_A,REPONSE_B,DATE_CREATION,COULEUR,MOT_DE_PASSE FROM utilisateur WHERE NOM_UTILISATEUR=:nom"
		requeteGetAssocMapJoueur = "SELECT ID_NIVEAU FROM NIVEAUXCREATION WHERE ID_USER=:id" #refaire selon les noms de Mathieu
		#requeteGetArmesUtilisees = "SELECT ID_ARME_PRINCIPALE, ID_ARME_SECONDAIRE FROM ARMEUTILISEES WHERE ID_USER=:id"
		requeteGetArmesUtilisateur = "SELECT ID_ARMES,QUANTITE FROM UTILISATEURS_ARMES WHERE ID_USER=:id" #Il faut creer la table associative 
		requeteGetNiveauxPreferes = "SELECT ID_NIVEAU FROM NIVEAUXFAVORIS WHERE ID_USER=:id" #et aussi une table pour arme ou pe utiliser celle qu'on a pour les parametres
		donnees = ma_connexion.executerSelectListe(requeteGetUtilisateurs,{":nom":username})
		if len(donnees) > 0:
			util = donnees[0]
			id_util = util[0]
			unUtilisateur={}
			unUtilisateur["ID"]=id_util
			unUtilisateur["NOM_UTILISATEUR"]=util[1]
			unUtilisateur["NOM"]=util[2]
			unUtilisateur["PRENOM"]=util[3]
			unUtilisateur["QUESTION_A"]=util[4]
			unUtilisateur["QUESTION_B"]=util[5]
			unUtilisateur["REPONSE_A"]=util[6]
			unUtilisateur["REPONSE_B"]=util[7]
			unUtilisateur["DATE_CREATION"]=str(util[8])
			unUtilisateur["COULEUR"]=util[9]
			unUtilisateur["MOT_DE_PASSE"]=util[10]
			#On donne au DTO la reference a toutes les armes que pocede le joueur
			liste_id_arme = ma_connexion.executerSelectListe(requeteGetArmesUtilisateur,{":id":id_util})
			unUtilisateur["EQUIPEMENT"] = DTOEquipement.getDictArmeUtilisateur(liste_id_arme)#retour une liste du genre {"nom_arme1":dict_arme1,"nom_arme2":dict_arme2...}
			#On donnee refence a l'arme principale et secondaire 
			#cur = ma_connexion.cursor()
			#cur.execute(requeteGetArmesUtilisees,{":id":id_util})
			#armeUtilise = cur.fetchall()
			unUtilisateur["ARME_PRINCIPALE"] = None
			unUtilisateur["ARME_SECONDAIRE"] = None
			#if len(armeUtilise) > 1:
			#	unUtilisateur["ARME_PRINCIPALE"] = DTOEquipement.getArmeParId(armeUtilise[0])
			#	unUtilisateur["ARME_SECONDAIRE"] = DTOEquipement.getArmeParId(armeUtilise[1])
			#cur.close()
			#On fait donne reference au maps que l'utilisateur a créé
			listeMap = ma_connexion.executerSelectListe(requeteGetAssocMapJoueur,{":id":id_util})
			listeIdMap = []
			for carte in listeMap: 
				listeIdMap.append(carte[0])
			unUtilisateur["MAPS"] = DTOMap.getMapsDunUtilisateur(listeIdMap)#retour une liste du genre {"nom_map1":dict_map1,"nom_map2":dict_map2...}
			#On fait donne reference au maps que l'utilisateur prefere

			listeMap = ma_connexion.executerSelectListe(requeteGetNiveauxPreferes,{":id":id_util})
			listeIdMap = []
			for carte in listeMap: 
				listeIdMap.append(carte[0])
			unUtilisateur["MAPS_PREFEREES"]=DTOMap.getMapsDunUtilisateur(listeIdMap) #Reference selon DTOMap et requete
			dto.ajouterUtilisateur(unUtilisateur)
		
		return dto

	def connecter(self,username):
		ma_connexion = Connexion()
		dtoUtil = None
		requete = "SELECT * FROM utilisateur WHERE NOM_UTILISATEUR=:1"
		info = ma_connexion.executerSelectListe(requete,[username])
		if len(info)>0:
			DTOMap = DAO_Map().lireDB()
			dtoEqui = DAOEquipement().lireDB()
			dtoUtil =  self.lireDB(DTOMap,dtoEqui,username)
		return dtoUtil

	def testerConnexion(self,username,psswrd):
		ma_connexion = Connexion()
		dtoUtil = None
		requete = "SELECT id, MOT_DE_PASSE FROM utilisateur WHERE NOM_UTILISATEUR=:1"
		info = ma_connexion.executerSelectListe(requete,[username])
		if len(info)>0:
			try:
				info = info[0]
				id = info[0]
				pw = info[1]
				pw = pw.replace("$2y$","$2a$")
				if bcrypt.hashpw(psswrd,pw) == pw:
					DTOMap = DAO_Map().lireDB()
					dtoEqui = DAOEquipement().lireDB()
					dtoUtil =  self.lireDB(DTOMap,dtoEqui,username)
					return dtoUtil
			except cx_Oracle.DatabaseError as er:
				error, = er.args
				print error
				print (u"Erreur de connexion")
		return None

	def updateArmeUtilisateur(self,idUser, dtoJoueur):
		ma_connexion = Connexion().meConnecter()
		info = None
		if ma_connexion != None:
			try:
				requeteGetArmesUtilisateur = "SELECT ID_ARMES,QUANTITE FROM UTILISATEURS_ARMES WHERE ID_USER=:id"
				cur = ma_connexion.cursor()
				cur.execute(requeteGetArmesUtilisateur,{":id":idUser})
				liste_id_arme = cur.fetchall()
				dtoJoueur.getUtilisateur()["EQUIPEMENT"] = DTOEquipement().getDictArmeUtilisateur(liste_id_arme) 
			except cx_Oracle.DatabaseError as er:
				error, = er.args
				print error
				print (u"Erreur de connexion")
				
				
	def selectIDUSER(self,x):
		ma_connexion = Connexion().meConnecter()
		IDUSERA = None
		if ma_connexion != None:
			try:
				requeteGet = "SELECT ID_USER FROM NIVEAUXCREATION WHERE ID_NIVEAU=:id"
				cur = ma_connexion.cursor()
				cur.execute(requeteGet,{":id":x})
				IDUSERA = cur.fetchall()
				#dtoJoueur.getUtilisateur()["EQUIPEMENT"] = DTOEquipement().getDictArmeUtilisateur(liste_id_arme) 
			except cx_Oracle.DatabaseError as er:
				error, = er.args
				print error
				print (u"Erreur de connexionaaaaaaaaaaaa")			
		return IDUSERA
		
	def selectIDUSER1(self,x):
		ma_connexion = Connexion().meConnecter()
		IDUSERAA = None
		if ma_connexion != None:
			try:
				requeteGet = "SELECT COUNT(ID_USER) FROM NIVEAUXFAVORIS WHERE ID_NIVEAU=:id"
				cur = ma_connexion.cursor()
				cur.execute(requeteGet,{":id":x})
				IDUSERAA = cur.fetchall()
				#dtoJoueur.getUtilisateur()["EQUIPEMENT"] = DTOEquipement().getDictArmeUtilisateur(liste_id_arme) 
			except cx_Oracle.DatabaseError as er:
				error, = er.args
				print error
				print (u"Erreur de connexion")
				
				
				
		return IDUSERAA
		
		
	def selectNOMUSER(self,x):
		ma_connexion = Connexion().meConnecter()
		NOMPROPRIO = None
		if ma_connexion != None:
			try:
				requeteGet = "SELECT NOM_UTILISATEUR FROM UTILISATEUR WHERE ID=:id"
				cur = ma_connexion.cursor()
				cur.execute(requeteGet,{":id":x})
				NOMPROPRIO = cur.fetchall()
				#dtoJoueur.getUtilisateur()["EQUIPEMENT"] = DTOEquipement().getDictArmeUtilisateur(liste_id_arme) 
			except cx_Oracle.DatabaseError as er:
				error, = er.args
				print error
				print (u"Erreur de connexion")
		return NOMPROPRIO
		
	def selectIDUSERF(self,x):
		ma_connexion = Connexion().meConnecter()
		requete = "UPDATE UTILISATEUR SET couleur=:1 WHERE id=:2"
		id1 = None
		if ma_connexion != None:
			try:
				requeteGet = "SELECT ID FROM MAPTT WHERE NOM=:id"
				cur = ma_connexion.cursor()
				cur.execute(requeteGet,{":id":x})
				id1 = cur.fetchall()
				#dtoJoueur.getUtilisateur()["EQUIPEMENT"] = DTOEquipement().getDictArmeUtilisateur(liste_id_arme) 
			except cx_Oracle.DatabaseError as er:
				error, = er.args
				print error
				print (u"Erreur de connexion")				
		return id1

	#Desoler pour le int au lieu du hexa mais en pratique ca avait laire incroyable mais pas le temps de changer
	#Il est 21h45
	def changerCouleur(self,couleur,id):
		ma_connexion = Connexion()
		couleur = int("0x"+str(couleur)+"000", 0)
		requete = "UPDATE UTILISATEUR SET couleur=:1 WHERE NOM_UTILISATEUR=:2"
		ma_connexion.executerInsertListe(requete,(couleur,id))