from ..connexionOracle.connexion_DB import Connexion
import cx_Oracle
from ..DTO.DTOHeatmap import Heatmap

class DAOHeatmap:
	
	def ecrireDansDB(self,dto):

		heatmapJ1 = dto.joueur1.getInfoPourDAO()
		heatmapJ2 = dto.joueur2.getInfoPourDAO()
		#print str(heatmapJ1)
		ma_connexion = Connexion()
		idPartie = dto.idPartie

		insertHM = """INSERT INTO DESCRIPTION_PARTIE(ID_PARTIE,ID_TUILE,QTE_DOMMAGE_TOTAL_RECU_J1,QTE_DOMMAGE_TOTAL_RECU_J2,
								QTE_DOMMAGE_TOTAL_DONNE_J1,QTE_DOMMAGE_TOTAL_DONNE_J2,TEMPS_PASSE_J1,TEMPS_PASSE_J2)
								SELECT :ID_PARTIE as ID_PARTIE , ID as id_tuile, :QTE_DOMMAGE_TOTAL_RECU_J1 as QTE_DOMMAGE_TOTAL_DONNE_J1,
								:QTE_DOMMAGE_TOTAL_RECU_J2 as QTE_DOMMAGE_TOTAL_DONNE_J2, :QTE_DOMMAGE_TOTAL_DONNE_J1 as QTE_DOMMAGE_TOTAL_DONNE_J1,
								:QTE_DOMMAGE_TOTAL_DONNE_J2 as QTE_DOMMAGE_TOTAL_DONNE_J2,:TEMPS_PASSE_J1 as TEMPS_PASSE_J1,:TEMPS_PASSE_J2 as TEMPS_PASSE_J2
								FROM valeurCase WHERE posx = :posx AND posy = :posy AND id_mapTT = :idMap"""

		valeursInsert = []
		for x in range(0,len(heatmapJ1)):
			caseJ1 = heatmapJ1[x]
			caseJ2 = heatmapJ2[x]
			valeursInsert.append({":ID_PARTIE":idPartie,":posx":caseJ2[0],":posy":caseJ2[1],":idMap":dto.idMap,":QTE_DOMMAGE_TOTAL_RECU_J1":caseJ1[3],":QTE_DOMMAGE_TOTAL_RECU_J2":caseJ2[3],
						":QTE_DOMMAGE_TOTAL_DONNE_J1":caseJ1[4],":QTE_DOMMAGE_TOTAL_DONNE_J2":caseJ2[4],":TEMPS_PASSE_J1":caseJ1[2],":TEMPS_PASSE_J2":caseJ2[2]})
		#print str(valeursInsert)
		ma_connexion.executemanyInsert(insertHM,valeursInsert)
