from ..connexionOracle.connexion_DB import Connexion
import cx_Oracle
from ..DTO.DTOStatArmes import DTOStatArmes

class DAOStatArmes:
	def ecireDansDB(self,dto):
		idj=dto.idJoueur
		liste_arme = ["Canon","Grenade","Mitraillette","Piege","Shotgun","Missile"]
		ma_connexion = Connexion()
		for arme in liste_arme:
			insert1 = "SELECT ID_ARME FROM ARMES WHERE NOM=:1"
			ida=ma_connexion.executerSelectListe(insert1,{":1":arme})
			if ida[0][0]==6:
				nom="Guide"
			else:
				nom=arme
			insert = "UPDATE DESCRIPTION_UTILISATEUR SET NB_FOIS_TIRE = NB_FOIS_TIRE+:1, QTE_DOMMAGE_TOTAL = QTE_DOMMAGE_TOTAL +:2 WHERE ID_UTILISATEUR = :3 AND ID_ARME= :4"
			ma_connexion.executerInsertListe(insert,{":1":dto.getArme(nom).nbFoisTire,":2":dto.getArme(nom).degat,":3":idj,":4":ida[0][0]})