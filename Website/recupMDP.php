<?php


	require_once("partial/header.php");
?>
    <!--Page d'accueil-->
<div class="loginDiv">
	<form action="recupMDP.php" onsubmit="return valider('prenom', 'nom', 'mdp', 'confmdp', 'questionA', 'reponseA', 'questionB', 'reponseB')"> <!--A changer-->
		<div class="loginFormDiv">

			<!-- Prenom -->
			<div class="loginLabel"><label for="prenom" id="lPrenom">Nom du joueur :</label></div> <!-- Si existe dans la table joueur, on propose les autres champs-->
			<div class="loginInput"><input type="text" name="prenom" id="prenom" /></div>
			<div class="loginSep"></div>

				<!-- nom -->
			<div class="loginLabel"><label for="reponseA" id="lNom">Réponse A :</label></div>
			<div class="loginInput"><input type="text" name="nom" id="nom" /></div>
			<div class="loginSep"></div>

			<div class="loginSep"></div>

				<!-- nom -->
			<div class="loginLabel"><label for="reponseB" id="lNom">Réponse B :</label></div>
			<div class="loginInput"><input type="text" name="nom" id="nom" /></div>
			<div class="loginSep"></div>

			<!-- mdp -->
			<div class="loginLabel"><label for="mdp" id="lMPD">Nouveau mdp :</label></div> <!-- Si les deux réponses secrètes match on propose de changer le mdp-->
			<div class="loginInput"><input type="password" name="mdp" id="mdp" /></div>
			<div class="clear"></div>
			<div class="loginLabel"><p>&nbsp;</p></div>
			<div class="descMDP">Min. de 9 caractères</div>
			<div class="loginSep"></div>

			<!-- confirmation mdp -->
			<div class="loginLabel"><label for="confmdp" id="lMPD">Répéter mdp :</label></div>
			<div class="loginInput"><input type="password" name="mdp" id="mdp" /></div>
			<div class="clear"></div>
			<div class="loginLabel"><p>&nbsp;</p></div>
			<div class="descMDP">Min. de 9 caractères</div>
			<div class="loginSep"></div>

			<div class="loginLabel">&nbsp;</div>
			<button type="submit" class="btn btn-info">Changer mot de passe</button>
			<button type="submit" class="boutonInscr">
				<!--<img src="images/btn-inscrire.jpg" alt="S'inscrire" />-->
			</button>
			<div class="clear"></div>
			<div class="loginSep"></div> 
		</div>
	</form>
	
</div>
</div>
<div id="accroche">Retrouvez votre mot de passe,<br> et vivez <br>la plus belle experience </div>
<div id="accroche2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;de votre vie. </div> 
<div class="clear"></div>
</div>
</body>
</html>