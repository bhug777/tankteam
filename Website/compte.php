<?php
	require_once("action/joueur.php");
	
	//require_once("action/priveAction.php");
	
	//execute();

	require_once("partial/header.php");

	if (!isset($_SESSION['joueur']))
	{
		header('Location: index.php');
		exit();
	}
	
	?>
	<div id="messageAccueil">
		<h2>Bonjour <?= $_SESSION['pseudo'] ?>!</h2>
		<br><br>
		<a href="logout.php"><button class="btn btn-primary">Logout</button></a>
		<a href="recupMDP.php" ><button class="btn btn-success">Changer mot de passe</button></a>
		<a href="info.php"><button class="btn btn-danger">Consulter</button></a>
		<!--var t = $("#element").spectrum("get");
		t.toRgbString() // "rgb(255, 0, 0)"--> <!--Récuperer le rgba choisi-->
	</div>
	</body>
</html>
