<?php
	require_once("action/joueur.php");
	session_start();
	require_once("action/changeColor.php");
	$niveauxPref=$_SESSION['joueur']->getNiveauxPref(); //array de niveaux
	$niveaux=$_SESSION['joueur']->getNiveaux();
	$armes= $_SESSION['joueur']->getArmes();
	$joueur=$_SESSION['joueur'];
	
	require_once("partial/header.php");

	if (!isset($_SESSION['joueur']))
	{
		header('Location: index.php');
		exit();
	}
	
	?>
	<div id="messageCompte">
	<h2 style="text-align:center;">Infos de <?= $_SESSION['joueur']->getPseudo() ?></h2>
	<br>

	 <table class="centered">
        <thead>
          <tr>
              <th data-field="id">Niveaux favoris</th>
              <th data-field="name">Niveau créé</th>
              <th data-field="price">Armurerie</th>
          </tr>
        </thead>

        <tbody>
        <tr>
        <td>
        <table>
          <?php
          	foreach ($joueur->getNomsMaps() as $key ) {
          		?>
          		<tr><td> <?php echo $key; ?></td></tr>
          	<?php
         	}
			?>
			</table >

		</td>
		<td>
		 <table>
		  <?php
          	foreach ($joueur->getNomsMaps() as $key ) {
          		?>
          		<tr><td> <?php echo $key; ?></td></tr>
          	<?php
         	}
			?>
			 </table>
			</td>
			<td>
			 <table>
		 <?php
          	foreach ($joueur->getNomsArmes() as $key ) {
          		?>
          		<tr><td> <?php echo $key; ?></td></tr>
          	<?php
         	}
			?>
			 </table>
			</td>
			</tr>
        </tbody>
      </table>
      		<br><br>
      		<form action="info.php" method="post">
      		<label style="margin-left:42px;">Couleur du tank : </label><input class="jscolor" name="color" id="color"><a>&nbsp;<button style="margin-left:10px;" class="btn btn-info" type="submit">Modifier couleur</button></a>
      		</form>
      		<br><br>
      	<div style="margin:auto;">
			<a href="logout.php"><button class="btn btn-primary">Logout</button></a>
			<a href="recupMDP.php" ><button class="btn btn-success">Changer mot de passe</button></a>
			<a href="info.php"><button class="btn btn-danger">Consulter</button></a>
			<form action="pagePublique.php" method="POST" style="display:inline-block;">
				<input type="hidden" name="joueurCherche" value="<?php echo $_SESSION['joueur']->getPseudo() ?>">
				<button type="submit" class="btn btn-warning">Ma page publique</button>
			</form>
		</div>
	</div>
	</body>
</html>
