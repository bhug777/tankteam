﻿
$( document ).ready(function(){
	$('#func').on('click', function(e){
		//e.preventDefault();
		var  form = $("#formulaireInfo");
		//On get  le id de la partie selectionne
		var option = document.getElementById("listeparties");
		var idPartie = option.options[option.selectedIndex].value;
		//On get les valeurs desirer a afficher
		var tabType = document.getElementsByName('type');
		for (var i = 0, length = tabType.length; i < length; i++) 
   			 if (tabType[i].checked) 
   			 	type=tabType[i].value;
		//On get les checkbox selectionne
		var joueur;
		var checkboxes = document.getElementsByName("joueur");
		var checkboxesChecked = [];
		 for (var i=0; i<checkboxes.length; i++) {
		     if (checkboxes[i].checked) {
		        checkboxesChecked.push(checkboxes[i]);
		     }
		}

			if(checkboxesChecked.length == 2)
				joueur = "both";
			else if(checkboxesChecked.length == 1)
				if(checkboxesChecked[0].value == "j1")
					joueur = "joueur1";
				else
					joueur = "joueur2";

		$.ajax({ url: 'action/ajax.php',
		        data: {func: 'hm', id:idPartie ,type:type , joueur:joueur},
		        type: 'post',
		        dataType: "json",
		        success: function(output) {
		           afficherHeatmap(output,joueur,type);
		        }
			});
	});
});

function afficherHeatmap(output,joueur,type)
{
	var tableHM = document.getElementById("myTable");
	var echelle = document.getElementById("contentEchelle");
	var arr = [];

	if(type=="temps"){
		if(joueur=="joueur1")
			echelle.innerHTML = "<div id='echelleJoueur1'><p>Temps passé par le joueur 1 </p></div>";
		else if(joueur=="joueur2")
			echelle.innerHTML = "<div id='echelleJoueur2'><p>Temps passé par le joueur 2 </p></div>";
		else if(joueur=="both")
		{
			echelle.innerHTML = "<div id='echelleJoueur1'><p>Temps passé par le joueur 1 </p></div>";
			echelle.innerHTML += "<div id='echelleJoueur2'><p>Temps passé par le joueur 2 </p></div>";
		}
	}
	else if(type=="dommage"){
		if(joueur=="joueur1")
			echelle.innerHTML = "<div id='echelleJoueur1'><p>Dommages reçus par le joueur 1 </p></div>";
		else if(joueur=="joueur2")
			echelle.innerHTML = "<div id='echelleJoueur2'><p>Dommages reçus par le joueur 2 </p></div>";
		else if(joueur=="both")
		{
			echelle.innerHTML = "<div id='echelleJoueur1'><p>Dommages reçus par le joueur 1 </p></div>";
			echelle.innerHTML += "<div id='echelleJoueur2'><p>Dommages reçus par le joueur 2 </p></div>";
		}
	}
	else if(type=="degats"){
		if(joueur=="joueur1")
			echelle.innerHTML = "<div id='echelleJoueur1'><p>Dégats donnés par le joueur 1 </p></div>";
		else if(joueur=="joueur2")
			echelle.innerHTML = "<div id='echelleJoueur2'><p>Dégats donnés par le joueur 2 </p></div>";
		else if(joueur=="both")
		{
			echelle.innerHTML = "<div id='echelleJoueur1'><p>Dégats donnés par le joueur 1 </p></div>";
			echelle.innerHTML += "<div id='echelleJoueur2'><p>Dégats donnés par le joueur 2 </p></div>";
		}
	}
	if(joueur=="both")
	{
		var hm = new Heatmap();
		var arr = hm.valeurCouleurAB(output[0],output[1]); 
	}
	else if(joueur=="joueur1")
	{
		var hm = new Heatmap();
		var arr = hm.valeurCouleurA(output); 

	}
	else if(joueur=="joueur2")
	{
		var hm = new Heatmap();
		var arr = hm.valeurCouleurB(output); 
	}
	console.log(arr);

	var content = ""
	for(var i = 0; i < arr.length; i++)
	{
		content  += '<tr style="height:50px;">';
		for(var j = 0; j < arr[i].length; j++)
		{
			content  += '<td style=" height:70px; width:50px; border: 1px solid black;background-color: rgb(' 
				+ Math.round(arr[i][j][0]) + "," + Math.round(arr[i][j][1]) + "," + Math.round(arr[i][j][2]) + ')"></td>';
		}
		content  += '</tr>';
	}
	tableHM.innerHTML = content;
}
