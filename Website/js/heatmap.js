function Heatmap(){}

Heatmap.prototype.arrPourcentage =function(arr2d)
{
	var arr1d = this.convertArrayTo1D(arr2d);
	var max = this.getMaxArr(arr1d);
	var temp1dArr = [];
	arr1d = arr1d.map(function(x){
		return x/max;
	});

	return arr1d;
}

//Rouge
Heatmap.prototype.valeurCouleurA = function(arr2dA)
{
	arr2dA = arr2dA;
	var arr1d = this.arrPourcentage(arr2dA);
	temp1dArr = arr1d.map(function(x){
		return [255,255-255*x,255-255*x];
	});
	//console.log(arr2dA);
	return this.convertArray2D(temp1dArr,arr2dA.length);
}

//Bleu
Heatmap.prototype.valeurCouleurB = function(arr2dB)
{
	arr2dB = arr2dB;
	var arr1d = this.arrPourcentage(arr2dB);
	temp1dArr = arr1d.map(function(x){
		return [255-255*x,255-255*x,255];
	});
	//console.log(arr2dB);
	return this.convertArray2D(temp1dArr,arr2dB.length);
}

Heatmap.prototype.valeurCouleurAB = function(arr2dA,arr2dB)
{
	var arrA = this.convertArrayTo1D(this.arrPourcentage(arr2dA));
	var arrB = this.convertArrayTo1D(this.arrPourcentage(arr2dB));
	var colDep = [255,255,255];
	var colA = [255,0,0];
	var colB = [0,0,255];
	var colAB = [0,255,0];

	var temp1 = [];
	var temp2 = [];
	var temp3 = [];

	for(j=0;j<arrA.length;j++)
	{
		temp1[j] = [];
		for(i=0;i<colDep.length;i++) temp1[j][i]=(colA[i]-colDep[i])*arrA[j]+colDep[i];
	}

	for(j=0;j<arrA.length;j++)
	{
		temp2[j] = [];
		for(i=0;i<colDep.length;i++) temp2[j][i]=(colAB[i]-colB[i])*arrA[j]+colB[i];
	}

	for(j=0;j<temp2.length;j++)
	{
		temp3[j] = [];
		for(i=0;i<colDep.length;i++) temp3[j][i]=(temp2[j][i]-temp1[j][i])*arrB[j]+temp1[j][i];
	}
	return this.convertArray2D(temp3,arr2dA.length);
}

Heatmap.prototype.convertArray2D = function(arr, len)
{
	var fArr2D = [];
	while(arr.length) 
		fArr2D.push(arr.splice(0,len));
	return fArr2D;
}

Heatmap.prototype.convertArrayTo1D = function(arr2d)
{
	var newArr = [];
	for(var i = 0; i < arr2d.length; i++)
	    newArr = newArr.concat(arr2d[i]);
	return newArr;
}

Heatmap.prototype.getMaxArr = function(arr)
{
	return Math.max.apply(Math, arr);
}