<?php

	require_once("action/signUPAction.php");

	$resultat = execute();
	//$mdp = executemdp();
	
	require_once("partial/header.php");
	if (isset($_SESSION['joueur'])) {
		require_once("compte.php");
	}

	else {
		?>

    <!--Page d'accueil-->
	<div class="loginDiv">
		<form action="inscription.php" method="post">
			<div class="loginFormDiv">

				<!-- Pseudo -->
				<div class="loginLabel"><label for="pseudo" id="lPseudo">Pseudo :</label></div>
				<div class="loginInput"><input type="text" name="pseudo" id="pseudo" /></div>
				<div class="loginSep"></div>
				<!-- Prenom -->
				<div class="loginLabel"><label for="prenom" id="lPrenom">Prénom :</label></div>
				<div class="loginInput"><input type="text" name="prenom" id="prenom" /></div>
				<div class="loginSep"></div>

				<!-- nom -->
				<div class="loginLabel"><label for="nom" id="lNom">Nom :</label></div>
				<div class="loginInput"><input type="text" name="nom" id="nom" /></div>
				<div class="loginSep"></div>

				<div class="loginSep"></div>

				<!-- mdp -->
				<div class="loginLabel"><label for="mdp" id="lMPD">Mot de passe :</label></div>
				<div class="loginInput"><input type="password" name="mdp" id="mdp" /></div>
				<div class="clear"></div>
				<div class="loginLabel"><p>&nbsp;</p></div>
				<div class="descMDP">Min. de 9 caractères</div>
				<div class="loginSep"></div>
				
				<!--
				- Minimum 9 lettres.
				- Au moins une letttre minuscule.
				- Au moins une lettre majuscule.
				- Au moins un chiffre.
				- Au moins un symbole. Supporter au moins ceux-ci: !@#$%?&*()-->

				<!-- confirmation mdp -->
				<div class="loginLabel"><label for="confmdp" id="lMPD">Répéter mot de passe :</label></div>
				<div class="loginInput"><input type="password" name="confmdp" id="confmdp" /></div>
				<div class="clear"></div>
				<div class="loginLabel"><p>&nbsp;</p></div>
				<div class="descMDP">Min. de 9 caractères</div>
				<div class="loginSep"></div>

					<!-- nom -->
				<div class="loginLabel"><label for="questionA" id="lNom">Question secrète A :</label></div>
				<div class="loginInput"><input type="text" name="questionA" id="questionA" /></div>
				<div class="loginSep"></div>

				<div class="loginSep"></div>
					<!-- nom -->
				<div class="loginLabel"><label for="reponseA" id="lNom">Réponse A :</label></div>
				<div class="loginInput"><input type="text" name="reponseA" id="reponseA" /></div>
				<div class="loginSep"></div>

				<div class="loginSep"></div>
					<!-- nom -->
				<div class="loginLabel"><label for="questionB" id="lNom">Question secrète B :</label></div>
				<div class="loginInput"><input type="text" name="questionB" id="questionB" /></div>
				<div class="loginSep"></div>

				<div class="loginSep"></div>
					<!-- nom -->
				<div class="loginLabel"><label for="reponseB" id="lNom">Réponse B :</label></div>
				<div class="loginInput"><input type="text" name="reponseB" id="reponseB" /></div>
				<div class="loginSep"></div>

				<div class="loginSep"></div>

				<div class="loginLabel">&nbsp;</div>
				<button type="submit" class="btn btn-success">Sign up</button>
				<div class="clear"></div>
				<?php if ($_POST){?><br><div class="SigninLabel">&nbsp;</div>
					<div class="descMDP"><strong style="color:red;font-size:15px;"><?php $_POST=array();$_SESSION=array(); ?></strong></div> <?php }else{}?>
				<div class="loginSep"></div>
				<div class="lienConn">
					<br>Déjà membre?
					<a href="index.php">Connectez-vous</a>
				</div>
				<div class="loginSep"></div> 
			</div>
		</form>
	</div>
	<?php
	}
	?>
<div id="accroche">Inscrivez-vous,<br> et vivez <br>la plus belle experience </div>
<div id="accroche2"> de votre vie. </div> 

<div class="clear"></div>
</div>
</body>
</html>