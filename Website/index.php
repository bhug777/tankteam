<?php
	require_once("action/indexAction.php");
    require_once("action/actionRecherche.php");
	$resultat = execute();
	$recherche= chercherJoueur();
	
	require_once("partial/header.php");
	if (isset($_SESSION['joueur'])) {
		
		require_once("compte.php");
	}
	else {
		?>
		<!--Page d'accueil-->
		<div class="loginDiv">
			<form action="index.php" method="post">
				<div class="loginFormDiv">

					<!-- nom -->
					<div class="SigninLabel"><label for="nom" id="lNom">Pseudo :</label></div>
					<div class="loginInput"><input type="text" name="pseudo" id="nom" /></div>
					<div class="loginSep"></div>

					<div class="loginSep"></div>

					<!-- mdp -->
					<div class="SigninLabel"><label for="mdp" id="lMPD">Mot de passe :</label></div>
					<div class="loginInput"><input type="password" name="mdp" id="mdp" /></div>
					<div class="clear"></div>
					<div class="loginSep"></div>
					<div class="SigninLabel">&nbsp;</div>
					<div class="lienConn">
						<a href="recupMDP.php">Mot de passe oublié?</a>
					</div>
					<div class="loginSep"></div>
					<div class="SigninLabel">&nbsp;</div>
					<button type="submit" class="btn btn-primary">Sign in</button>
					<a href="mailto:support@lemeilleurservicealaclienteledevotrevie.com?Subject=Problems" class="btn btn-info">Support</a>
					<div class="clear"></div>
					<?php if (isset($_POST["pseudo"]) && isset($_POST["mdp"])){?><br><div class="SigninLabel">&nbsp;</div>
					<div class="descMDP"><strong style="color:red;font-size:15px;"><?php $_POST=array();$_SESSION=array(); echo $resultat; ?></strong></div> <?php }else{}?>
					<div class="loginSep"></div>
					<div class="SigninLabel">&nbsp;</div>
					<div class="lienConn">
						<br>Pas membre?
						<a href="inscription.php">Inscrivez-vous</a>
					</div>
				</div>
			</form>
			</div>
		<?php
	}
	?>

<div id="accroche">Connectez-vous,<br> et vivez <br>la plus belle experience </div>
<div id="accroche2"> de votre vie. </div> 

<div class="clear"></div>
</div>
</body>
</html>


