<?php

//Nom de joueur unique, a verfier
//Contrainte de mdp
require_once("joueurInscription.php");
session_start();
function execute() {
	if (!empty($_POST["pseudo"]) && !empty($_POST["mdp"]) && !empty($_POST["prenom"]) && !empty($_POST["nom"]) && 
		!empty($_POST["confmdp"]) && !empty($_POST["questionA"]) && !empty($_POST["reponseA"]) && !empty($_POST["questionB"]) && !empty($_POST["reponseB"])){
		
		/*$rawdata=exec('"C:\Logiciels Portables\Panda3D-1.9.1-x64\python\ppython.exe" ..\Tools\src\create_DTOUtilisateur_from_TXT.py '.);
		$encoderesult=json_decode($rawdata);*/		

		if (valider_pass($_POST["mdp"])==FALSE) {
			return "Erreur: Le mot de passe doit contenir au moins: 1 majuscule, 1 minuscule, 1 caractère spécial,
			 1 chiffre, et avoir une longueur d'au moins 9 caractères."; 
		}
		elseif ($_POST["confmdp"]!= $_POST["mdp"]) {
			return "Les deux mots de passe ne sont pas les mêmes. Stay focused."; //Confirmation du mot de passe pas bonne
		}
		else {
			$_SESSION['pseudo'] = $_POST["pseudo"];
			$_SESSION['mdp'] = password_hash($_POST["mdp"], PASSWORD_BCRYPT);
			$joueur= new JoueurInscription($_POST['nom'], $_POST['prenom'],$_SESSION['pseudo'],$_POST['questionA'],$_POST['reponseA'],$_POST['questionB'],$_POST['reponseB'],$_SESSION['mdp']);
			$_SESSION['joueur2'] = $joueur;
			$ajout =  str_replace('"',"'",$joueur->creationJSON());
			exec('"C:\Logiciels Portables\Panda3D-1.9.1-x64\python\ppython.exe" ..\Tools\src\create_DTOUtilisateur_from_TXT.py "'.$ajout.'"');
			return $_SESSION['joueur2'];
		}
	}
	else {
		return "Veuillez remplir tous les champs"; 
	}
}

function valider_pass($candidate) { //Pour l'inscription et la recuperation du mdp
   $r1='/[A-Z]/';  //Majuscule
   $r2='/[a-z]/';  //Minuscule
   $r3='/[!@#$%^&*()\-_=+{};:,<.>]/';  //Caractères spéciaux
   $r4='/[0-9]/';  //Nombres

   if(preg_match_all($r1,$candidate, $o)<1) return FALSE;

   if(preg_match_all($r2,$candidate, $o)<1) return FALSE;

   if(preg_match_all($r3,$candidate, $o)<1) return FALSE;

   if(preg_match_all($r4,$candidate, $o)<1) return FALSE;

   if(strlen($candidate)<9) return FALSE;

   return TRUE;
}
