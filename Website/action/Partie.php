<?php

class Partie{
    private $pseudo;
    private $joueur1;
    private $joueur2;
    private $resultatPartie;
    private $carteNiveauJoue=array();

    function __construct($pseudo,$nomRep,$txVictoire,$totVictoires,$totDefaites,$totNulles,$totNiveauxCrees,$armeFavorite,$dommageMoyenArme1,$dommageMoyenArme2,$dommageMoyenArme3,$dommageMoyenParTir,$nbPartiesJouees) {
        $this->pseudo=$pseudo,
        $this->nomRep=$nomRep,
        $this->txVictoire=$txVictoire,
        $this->totVictoires=$totVictoires,
        $this->totDefaites=$totDefaites,
        $this->totNulles=$totNulles,
        $this->totNiveauxCrees=$totNiveauxCrees,
        $this->armeFavorite=$armeFavorite,
        $this->dommageMoyenArme1=$dommageMoyenArme1,
        $this->dommageMoyenArme2=$dommageMoyenArme2,
        $this->dommageMoyenArme3=$dommageMoyenArme3,
        $this->dommageMoyenParTir=$dommageMoyenParTir,
        $this->nbPartiesJouees=$nbPartiesJouees,
    }

     public function getPseudo()
    {
        return $this->pseudo;
    }

     public function getNomRep()
    {
        return $this->nomRep;
    }
?>