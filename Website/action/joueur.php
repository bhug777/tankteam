<?php

class Joueur{
	private $nom;
	private $prenom;
	private $pseudo;
	private $niveauxPref;
	private $niveauxCrees;
	private $armesPrimaires;
	private $armesSecondes;
	private $armes;
    private $color;
    private $questionA;
    private $reponseA;
    private $questionB;
    private $reponseB;
    private $mdp;

	function __construct($nom, $prenom,$pseudo,$niveauxPref, $armesPrimaires,$armesSecondes, $armes, $niveaux, $color) {
		$this->nom=$nom;
		$this->prenom=$prenom;
		$this->pseudo=$pseudo;
		$this->niveauxPref=$niveauxPref;
		$this->armesPrimaires=$armesPrimaires;
		$this->armesSecondes=$armesSecondes;
		$this->armes=$armes;
		$this->niveaux=$niveaux;
        $this->color=$color;
	}

	 public function getNom()
    {
        return $this->nom;
    }

     public function getPrenom()
    {
        return $this->prenom;
    }

    public function getPseudo()
    {
        return $this->pseudo;
    }

    public function getNiveauxPref()
    {
        return $this->niveauxPref;
    }

    public function getNiveauxCrees()
    {
        return $this->niveauxCrees;
    }

    public function getNiveaux()
    {
        return $this->niveaux;
    }

    public function getArmesPrimaires()
    {
        return $this->armesPrimaires;
    }

    public function getArmesSecondes()
    {
        return $this->armesSecondes;
    }

    public function getArmes()
    {
        return $this->armes;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getNomsMaps()
    {
        $liste = array();
         foreach($this->niveaux as $key => $jsons)
         {
           array_push($liste, $key);
         }
         return $liste;
   }

   public function getNomsArmes()
    {
        $liste = array();
        if ($this->armes!=NULL){

             foreach($this->armes as $key => $jsons)
             {
               array_push($liste, $key);
             }
        }
         return $liste;
   }
}
?>