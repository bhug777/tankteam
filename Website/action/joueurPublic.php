<?php

class JoueurPublic{
    private $pseudo;
    private $nomRep;
    private $txVictoire;
    private $abandons;
    private $totVictoires;
    private $totDefaites;
    private $totNulles;
    private $totNiveauxCrees;
    private $armeFavorite;
    private $dommageMoyenArme;
    private $dommageMoyenParTir;
    private $nbPartiesJouees;
    private $partiesJouees;
    private $idJoueur;

    function __construct($pseudo,$nomRep,$txVictoire,$abandons,$totVictoires,$totDefaites,
        $totNulles,$totNiveauxCrees,$armeFavorite,$dommageMoyenArme,$dommageMoyenParTir,$nbPartiesJouees, $partiesJouees,$idJoueur) {

        $this->idJoueur = $idJoueur;

        if( !empty($pseudo))
            $this->pseudo=$pseudo;
        else
            $this->$pseudo="NO DATA";

        if( $nomRep!=NULL)
            $this->nomRep=$nomRep;
        else
            $this->nomRep="NO DATA";

        if( !empty($txVictoire))
            $this->txVictoire=$txVictoire;
        else
            $this->txVictoire="0%";

        if( !empty($abandons))
            $this->abandons=$abandons;
        else
            $this->abandons="0";

        if( !empty($totVictoires))
            $this->totVictoires=$totVictoires;
        else
            $this->totVictoires="0";

        if( !empty($partiesJouees))
            $this->partiesJouees=$partiesJouees;
        else
            $this->partiesJouees="NO DATA";
        
        if( !empty($totDefaites))
            $this->totDefaites=$totDefaites;
        else
            $this->totDefaites="0";

        if( !empty($totNulles))
            $this->totNulles=$totNulles;
        else
            $this->totNulles="0";

        if( !empty($totNiveauxCrees))            
            $this->totNiveauxCrees=$totNiveauxCrees;
        else
            $this->totNiveauxCrees="0";

        if( !empty($armeFavorite))
            $this->armeFavorite=$armeFavorite[0]["NOM"];
        else{
            $this->armeFavorite="NO DATA";
        }

        if( !empty($dommageMoyenArme)){
            $this->dommageMoyenArme=$dommageMoyenArme;
        }
        else{
            $this->dommageMoyenArme="0";
        }

        if( !empty($dommageMoyenParTir)){
            $this->dommageMoyenParTir=$dommageMoyenParTir;
        }
        else
            $this->dommageMoyenParTir="0";

        if( !empty($nbPartiesJouees))
            $this->nbPartiesJouees=$nbPartiesJouees;
        else
            $this->nbPartiesJouees="0";

    }

     public function getPseudo()
    {
        return $this->pseudo;
    }

     public function getNomRep()
    {
        return $this->nomRep;
    }

    public function getTxVictoire()
    {
        return $this->txVictoire;
    }

    public function getAbandons()
    {
        return $this->abandons;
    }

    public function getTotVictoires()
    {
        return $this->totVictoires;
    }

    public function getTotDefaites()
    {
        return $this->totDefaites;
    }

    public function getTotNulles()
    {
        return $this->totNulles;
    }

    public function getTotNiveauxCrees()
    {
        return $this->totNiveauxCrees;
    }

    public function getArmeFavorite()
    {
        return $this->armeFavorite;
    }

     public function getDommageMoyenArme()
    {
        $tabArme = array();
        $index = 0;
        foreach($this->dommageMoyenArme as $arme)
        {
            $tabArme[$index] = $arme[0]." -> ".round($arme[1],2);
            $index += 1;
        }
        return $tabArme;
    }

    public function getDommageMoyenParTir()
    {
        return $this->dommageMoyenParTir;
    }

    public function getNbPartiesJouees()
    {
        return $this->partiesJouees;
    }

    public function getPartiesJouees()
    {  
        $first = true;
        $content = "";
        foreach($this->nbPartiesJouees as $partie)
        {
            if($first){
                $isCheck = "selected";
                $first = false;
            }
            else
                $isCheck = "";

            if($partie["ID_GAGNANT"] == $this->idJoueur){
                $content = $content."<option ".$isCheck." id='partie-".$partie["ID"]."' style='background-color:rgba(39, 174, 96,1.0);' value='".$partie["ID"]."'>".$partie[3]." vs ".$partie["NOM_UTILISATEUR"]."</option>";
            }
            else if($partie["ID_GAGNANT"] == 0){
                //Null
            }
            else if($partie["ID_GAGNANT"] != $this->idJoueur)
                $content = $content."<option ".$isCheck." id='partie-".$partie["ID"]."' style='background-color:rgba(211, 84, 0,1.0);' value='".$partie["ID"]."'>".$partie["NOM_UTILISATEUR"]." vs ".$partie[3]."</option>";
        } 
        return $content;
    }

    /*public function getNomsMaps()
    {
        $liste = array();
         foreach($this->niveaux as $key => $jsons)
         {
           array_push($liste, $key);
         }
         return $liste;
   }

   public function getNomsArmes()
    {
        $liste = array();
        if ($this->armes!=NULL){

             foreach($this->armes as $key => $jsons)
             {
               array_push($liste, $key);
             }
        }
         return $liste;
   }*/
}
?>