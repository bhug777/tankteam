<?php

class JoueurInscription{
    private $nom;
    private $prenom;
    private $pseudo;
    private $color;
    private $questionA;
    private $reponseA;
    private $questionB;
    private $reponseB;
    private $mdp;

    function __construct($nom, $prenom,$pseudo,$questionA,$reponseA,$questionB,$reponseB,$mdp) {
        $this->nom=$nom;
        $this->prenom=$prenom;
        $this->pseudo=$pseudo;
        $this->questionA=$questionA;
        $this->reponseA=$reponseA;
        $this->questionB=$questionB;
        $this->reponseB=$reponseB;
        $this->mdp=$mdp;
    }

    public function creationJSON(){
        $objet=array();
        $objet["NOM"]=$this->nom;
        $objet["PRENOM"]=$this->prenom;
        $objet["NOM_UTILISATEUR"]=$this->pseudo;
        $objet["COULEUR"]=255255255255;
        $objet["QUESTION_A"]=$this->questionA;
        $objet["REPONSE_A"]=$this->reponseA;
        $objet["QUESTION_B"]=$this->questionB;
        $objet["REPONSE_B"]=$this->reponseB;
        $objet["MOT_DE_PASSE"]=str_replace("$", "!", $this->mdp);
        return json_encode($objet);
    }
    
     public function getNom()
    {
        return $this->nom;
    }

     public function getPrenom()
    {
        return $this->prenom;
    }

    public function getPseudo()
    {
        return $this->pseudo;
    }

    public function getNiveauxPref()
    {
        return $this->niveauxPref;
    }

    public function getNiveauxCrees()
    {
        return $this->niveauxCrees;
    }

    public function getNiveaux()
    {
        return $this->niveaux;
    }

    public function getArmesPrimaires()
    {
        return $this->armesPrimaires;
    }

    public function getArmesSecondes()
    {
        return $this->armesSecondes;
    }

    public function getArmes()
    {
        return $this->armes;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getNomsMaps()
    {
        $liste = array();
         foreach($this->niveaux as $key => $jsons)
         {
           array_push($liste, $key);
         }
         return $liste;
   }

   public function getNomsArmes()
    {
        $liste = array();
        if ($this->armes!=NULL){

             foreach($this->armes as $key => $jsons)
             {
               array_push($liste, $key);
             }
        }
         return $liste;
   }
}
?>