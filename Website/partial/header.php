<?php 
    require_once("DAO/DAOStatistique.php");
    ?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Accueil | Tankem</title>
	<link href='https://fonts.googleapis.com/css?family=Bangers' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <script src='spectrum.js'></script>
    <script src="colorpicker/jscolor.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Alfa+Slab+One' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
    <script src="js/hottie.js"></script>
    <script type="text/javascript" src="js/ajax.js"></script>
    <link href="css/styles.css" rel="stylesheet"/>
          
</head>
<body>
   <header id="navBandeau" >
        <!--menu-->
        <nav tabindex="0" >
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <li><a href="index.php">Connexion</a></li>
                <li><a href="inscription.php">Inscription</a></li>
                <li><a href="mailto:support@lemeilleurservicealaclienteledevotrevie.com?Subject=Problems">Contact</a></li>
                <li><div class="form-group">
                <form method="post" action="pagePublique.php">
                  <input style="margin-top:-7px;" type="text" name="joueurCherche" class="form-control" placeholder=" Chercher Joueur">
                </div></li>
                <li><button style="margin-top:-7px;" type="submit" class="btn btn-default">Go !</button></li>
                </form>

        <li class="infosParties">Parties débutées dans la dernière heure: <?php echo DAOStatistique::getNbPartieDernHeure() ?></li>
        <li class="infosParties">Nb total de parties jouées: <?php echo DAOStatistique::getCompteTousLesParties() ?></li>


        <li>
            </ul>
        </nav>
    </header>
    <div class="clear"></div>
