<?php
	session_start();

	$pseudo = $_SESSION['pseudo'];
	

	
	if (!isset($_SESSION['joueur']))
	{
		//unset des variables
		$_SESSION = array();
		header('Location: index.php');
		exit();
	}
	else{
		//cookies
		if (ini_get("session.use_cookies"))
		{
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 3600,
				$params['path'], $params['domain'],
				$params['secure'], $params['httponly']);
		}
		
		//detruire données côté serveur
		session_destroy();

		require_once("partial/header.php");

		?>
		<div id="messageAccueil">
			<h2 style="text-align:center;">Au revoir <?= $pseudo ?>!</h2>
			<br><br><br>
			<a href="index.php" style="margin-left:8em;"><button class="btn btn-primary">Retour accueil</button></a>
		</div>
		</body>
		</html>
	<?php
	} ?>
