<?php require_once("partial/header.php");
require_once("DAO/DAOStatistique.php");
require_once("DAO/DAOheatmap.php");
require_once("action/joueurpublic.php"); 
?>
<!--Page d'accueil-->
	<div id="messagePublic">
		<form id="formulaireInfo">
			<div class="loginFormDiv">
			<?php
				$joueurRecherche = null;
				if(isset($_POST['joueurCherche']))
					$joueurRecherche = DAOStatistique::getJoueurParNom($_POST['joueurCherche']);

				if(!empty($joueurRecherche)){
						$joueurPublic = new JoueurPublic($_POST['joueurCherche'],$joueurRecherche["reputation"],$joueurRecherche["ratioGagner"],$joueurRecherche["ratioAbandon"],$joueurRecherche["gagner"],$joueurRecherche["perdu"],
						$joueurRecherche["null"],$joueurRecherche["nbNivCree"],$joueurRecherche["armeFav"],$joueurRecherche["dmgMoyen"],$joueurRecherche["dmgMoyenTir"],$joueurRecherche["parties"], $joueurRecherche["partiesJouees"],$joueurRecherche["id"]);
						?>
						<h2 style="text-align:center;">Page publique de <?php echo $joueurPublic->getPseudo() ?></h2>
						<br>
						<br>
						<!-- Pseudo -->
						<div class="loginLabel"><label for="pseudo" id="lPseudo">Nom de réputation :</label></div>
						<div class="loginInput"><?php echo $joueurPublic->getNomRep() ?></div>
						<div class="loginSep"></div>
						<!-- Prenom -->
						<div class="loginLabel"><label for="prenom" id="lPrenom">Taux de gagnant/perdant :</label></div>
						<div class="loginInput"><?php echo round(($joueurPublic->getTxVictoire()),2) ?></div>
						<div class="loginSep"></div>

						<!-- nom -->
						<div class="loginLabel"><label for="nom" id="lNom">Taux d'abandon :</label></div>
						<div class="loginInput"><?php echo $joueurPublic->getAbandons() ?></div>
						<div class="loginSep"></div>

						<div class="loginSep"></div>

						<!-- mdp -->
						<div class="loginLabel"><label for="mdp" id="lMPD">Parties gagnées/perdues/nulles/totales :</label></div>
						<div class="loginInput"><?php echo $joueurPublic->getTotVictoires()."/".$joueurPublic->getTotDefaites()."/".$joueurPublic->getTotNulles()."/".$joueurPublic->getNbPartiesJouees() ?></div>
						<div class="clear"></div>
						<div class="loginLabel"><p>&nbsp;</p></div>
						<div class="loginSep"></div>
						
						<div class="loginLabel"><label for="confmdp" id="lMPD">Nombre total de niveaux crées :</label></div>
						<div class="loginInput"><?php echo $joueurPublic->getTotNiveauxCrees() ?></div>
						<div class="clear"></div>
						<div class="loginLabel"><p>&nbsp;</p></div>
						<div class="loginSep"></div>

							<!-- nom -->
						<div class="loginLabel"><label for="questionA" id="lNom">Arme la plus utilisée :</label></div>
						<div class="loginInput"><?php echo $joueurPublic->getArmeFavorite() ?></div>
						<div class="loginSep"></div>

						<div class="loginSep"></div>
							<!-- nom -->
						<div class="loginLabel"><label for="reponseA" id="lNom" style="height:90px;">Dommage moyen par arme :</label></div>
						<div class="loginInput"><?php echo $joueurPublic->getDommageMoyenArme()[1] ?></div>
						<br>
						<div class="loginInput"><?php echo $joueurPublic->getDommageMoyenArme()[2] ?></div>
						<br>
						<div class="loginInput"><?php echo $joueurPublic->getDommageMoyenArme()[3] ?></div>
						<br>
						<div class="loginInput"><?php echo $joueurPublic->getDommageMoyenArme()[4] ?></div>
						<br>
						<div class="loginInput"><?php echo $joueurPublic->getDommageMoyenArme()[5] ?></div>
						<br>
						<div class="loginInput"><?php echo $joueurPublic->getDommageMoyenArme()[6] ?></div>
						<div class="loginSep"></div>

						<div class="loginSep"></div>
							<!-- nom -->
						<div class="loginLabel"><label for="questionB" id="lNom">Dommage moyen par tir :</label></div>
						<div class="loginInput"><?php echo $joueurPublic->getDommageMoyenParTir() ?></div>
						<div class="loginSep"></div>
						<div class="loginLabel"><label for="exampleSelect2" style="height:100px;">Parties jouées :</label></div>
						  <div class="loginInput">
						  	<fieldset class="form-group">
							    <select multiple class="form-control" id="listeparties">
									<?php echo $joueurPublic->getPartiesJouees()?>
						   		</select>
						    </fieldset>
						    <div class="loginInput">
							    <div class="radio">
								  <label><input type="radio" name="type" value="temps" checked>Temps</label>
								</div>
								<div class="radio">
								  <label><input type="radio" name="type" value="degats">Dégats</label>
								</div>
								<div class="radio ">
								  <label><input type="radio" name="type" value="dommage">Dommages</label>
								</div>
								<br>
								<div class="checkbox">
							    <label>
							      <input type="checkbox" name="joueur" value="j1"> Joueur 1
							    </label>
							  </div>
							  <div class="checkbox" style="margin-left:20%;">
							    <label>
							      <input type="checkbox" name="joueur" value="j2" data-nom=""> Joueur 2
							    </label>
							  </div><br>
							  <br>
							</div>
						    <br>
						    <button id="func" type="button" class="btn btn-default" style="width:100%;">Infos Partie</button>
							<div class="loginSep"></div>
					 	 </fieldset>
					 	</div>
					 	<div id="contentEchelle"></div>
					 	<div class="loginSep"></div>
					 	<table id="myTable" style="background-color:white">
						</table>
						<div class="loginSep"></div>
						</div>
				</form>
				<?php }else {?>
					<h2 style="text-align:center;">Ce joueur n'existe pas :'( </h2>
					<?php }?>
			</div>
			<div id="heatmap"></div>
	</body>
	<script type="text/javascript" src="js/heatmap.js"></script>
	</html>