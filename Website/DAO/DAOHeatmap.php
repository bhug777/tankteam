<?php
	require_once("connexion.php");
	class DAOHeatmap
	{
		static function getHeatmapPartie($idPartie,$type,$joueur)
		{
			$response = array();
			$array_heatmap = array();

			$selectHeatmap = "SELECT DESCRIPTION_PARTIE.QTE_DOMMAGE_TOTAL_RECU_J1,DESCRIPTION_PARTIE.QTE_DOMMAGE_TOTAL_RECU_J2,
								DESCRIPTION_PARTIE.QTE_DOMMAGE_TOTAL_DONNE_J1,DESCRIPTION_PARTIE.QTE_DOMMAGE_TOTAL_DONNE_J2,
								DESCRIPTION_PARTIE.TEMPS_PASSE_J1,DESCRIPTION_PARTIE.TEMPS_PASSE_J2,VALEURCASE.posx,VALEURCASE.posy
							FROM DESCRIPTION_PARTIE 
							JOIN VALEURCASE ON (SELECT ID_NIVEAU FROM PARTIE WHERE ID = :idPartie) = VALEURCASE.ID_MAPTT
								AND DESCRIPTION_PARTIE.ID_TUILE = VALEURCASE.ID 
							WHERE ID_PARTIE = :idPartie
							ORDER BY VALEURCASE.ID ASC";

			
			Connection::seConnecter();
			$heatmap = Connection::executerSelect($selectHeatmap,[":idPartie"=>$idPartie]);

			foreach($heatmap as $row)
			{	
				$array_heatmap["joueur1"]["temps"][$row["POSX"]][$row["POSY"]] = $row["TEMPS_PASSE_J1"];
				$array_heatmap["joueur2"]["temps"][$row["POSX"]][$row["POSY"]] = $row["TEMPS_PASSE_J2"];
				$array_heatmap["joueur1"]["dommage"] [$row["POSX"]][$row["POSY"]] = $row["QTE_DOMMAGE_TOTAL_RECU_J1"];
				$array_heatmap["joueur2"]["dommage"] [$row["POSX"]][$row["POSY"]] = $row["QTE_DOMMAGE_TOTAL_RECU_J2"];
				$array_heatmap["joueur1"]["degats"][$row["POSX"]][$row["POSY"]] = $row["QTE_DOMMAGE_TOTAL_DONNE_J1"];
				$array_heatmap["joueur2"]["degats"][$row["POSX"]][$row["POSY"]] = $row["QTE_DOMMAGE_TOTAL_DONNE_J2"];
			}

			if($joueur == "joueur1")
				$response = $array_heatmap["joueur1"][$type];
			else if($joueur == "joueur2")
				$response = $array_heatmap["joueur2"][$type];
			else if($joueur == "both")
			{
				$response[0] = $array_heatmap["joueur1"][$type];
				$response[1] = $array_heatmap["joueur2"][$type];
			}

			return $response;
		}
	}

?>