<?php  

if(file_exists("DAO/infoConnexion.php"))
	require_once("DAO/infoConnexion.php");
//Pour le ajax
else if(file_exists("../DAO/infoConnexion.php"))
	require_once("../DAO/infoConnexion.php");


class Connection {
	private static $connection = null;

	// singleton
	public static function seConnecter() {
		if (empty(Connection::$connection)) {
			Connection::$connection = new PDO('oci:dbname=//'.DB_ALIAS.';charset=AL32UTF8', DB_USER, DB_PASS);
			//permet de lancer les erreurs
			Connection::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			//Endroit où les variables sont remplacés ? to var
			Connection::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		}
	}


	/* Pas nécessaire car quand on ferme la page, la connexion est fermée aussi*/
	public static function closeConnection() {
		if (!empty(Connection::$connection)) {	
			Connection::$connection	= null;
		}
	}

	public static function executerSelect($requete,$valeur)
	{
		$prep = Connection::$connection->prepare($requete);
		if($valeur)
		{
			$donnee = $prep->execute($valeur);
		}
		else
		{
			$prep->execute();
		}
		return $prep->fetchall();
	}
}
