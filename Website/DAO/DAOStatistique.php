<?php
	require_once("connexion.php");
	class DAOStatistique
	{
		static function getNbPartieDernHeure()
		{
			Connection::seConnecter();
			$requete = "SELECT count(id) FROM PARTIE WHERE SYSDATE - DATE_CREATION < 1/24";
			$donnee = Connection::executerSelect($requete,null);
			return $donnee[0]["COUNT(ID)"];
		}

		static function getCompteTousLesParties()
		{
			Connection::seConnecter();
			$requete = "SELECT count(id) FROM PARTIE";
			$donnee = Connection::executerSelect($requete,null);
			return $donnee[0]["COUNT(ID)"];
		}

		static function getJoueurParNom($nomJoueur)
		{
			$response = null;
			Connection::seConnecter();
			//On get le id du joueur demande
			$requeteGetIdJoueur = "SELECT ID,REPUTATION,NB_PARTIE_JOUE,NB_PARTIE_GAGNE,NB_PARTIE_PERDU,NB_PARTIE_ABANDON FROM UTILISATEUR WHERE NOM_UTILISATEUR = :nomJoueur";
			$donnee = Connection::executerSelect($requeteGetIdJoueur,[":nomJoueur"=>$nomJoueur]);

			if(count($donnee)>0)
			{
				$response = array();
				$idJoueur = $donnee[0]["ID"];
				$reputation = $donnee[0]["REPUTATION"];
				//On get le nombre de partie null de l'usager
				//$requetePartieNull = "SELECT COUNT(*) FROM PARTIE WHERE (ID_J1 = :id OR ID_J2 = :id) AND PARTIE_NUL != NULL";
				$nbNull=$donnee[0]["NB_PARTIE_ABANDON"];
				//On get le nombre de partie gagne de l'usager
				//$requetePartieGagner = "SELECT COUNT(*) FROM PARTIE WHERE ID_GAGNANT = :id";
				$nbGagner = $donnee[0]["NB_PARTIE_GAGNE"];
				//On get le nombre de partie perdu de l'usager
				//$requetePartiePerdu = "SELECT COUNT(*) FROM PARTIE WHERE (ID_J1 = :id OR ID_J2 = :id) ID_GAGNANT != :id";
				$nbPerdu = $donnee[0]["NB_PARTIE_PERDU"];
				//On get le nombre de niveau creer par l'usager
				$requeteMapCreer = "SELECT COUNT(*) FROM NIVEAUXCREATION WHERE ID_USER = :id";
				$nbNiveauCreer =  Connection::executerSelect($requeteMapCreer,[":id"=>$idJoueur])[0]["COUNT(*)"];
				//On get l'arme le plus utilisé
				$requeteArmeUtil = "SELECT DESCRIPTION_UTILISATEUR.ID_ARME,ARMES.NOM FROM DESCRIPTION_UTILISATEUR
									JOIN ARMES ON ARMES.ID_ARME = DESCRIPTION_UTILISATEUR.ID_ARME
									 WHERE NB_FOIS_TIRE = 
									(SELECT MAX(NB_FOIS_TIRE) FROM DESCRIPTION_UTILISATEUR) AND ID_UTILISATEUR = :id";
				$armeFav = Connection::executerSelect($requeteArmeUtil,[":id"=>$idJoueur]);
				//On get les dommages moyens par arme
				//Possibilité d'avoir plus d'une arme préférée
				$requeteDegatMoyen = "SELECT DESCRIPTION_UTILISATEUR.ID_ARME,ARMES.NOM, DESCRIPTION_UTILISATEUR.QTE_DOMMAGE_TOTAL,DESCRIPTION_UTILISATEUR.NB_FOIS_TIRE
										FROM DESCRIPTION_UTILISATEUR
										JOIN ARMES ON ARMES.ID_ARME = DESCRIPTION_UTILISATEUR.ID_ARME
										WHERE ID_UTILISATEUR = :id";
				$armeDegat = Connection::executerSelect($requeteDegatMoyen,[":id"=>$idJoueur]);
				//On get le nombre de partie joue par l'usager
				$nbPartieJouer = $donnee[0]["NB_PARTIE_JOUE"];
				//On trouve les ratio gagner 
				if($nbPerdu==0)
					$ratioGagner = $nbGagner;
				else
					$ratioGagner = $nbGagner/$nbPerdu;
				//On trouve les ratio perdu 
				if($nbGagner==0)
					$ratioPerdu = $nbPerdu;
				else
					$ratioPerdu = $nbPerdu/$nbGagner;
				//On trouve le ratio abandon
				if($nbGagner+$nbPerdu==0)
	
					$ratioAbandon = $nbNull;
				else
					$ratioAbandon = $nbNull/($nbGagner+$nbPerdu);
				//On get toutes les parties jouees par l'usager

				$requeteGetParties = "SELECT P.ID,P.ID_GAGNANT,M.NOM,U1.NOM_UTILISATEUR,U2.NOM_UTILISATEUR FROM PARTIE P
										JOIN MAPTT M ON M.ID = P.ID_NIVEAU
										JOIN UTILISATEUR U1 ON U1.ID = P.ID_J1
										JOIN UTILISATEUR U2 ON U2.ID = P.ID_J2
										WHERE (P.ID_J1 = :ID OR P.ID_J2 = :ID) ORDER BY P.DATE_CREATION ASC";
				$tousLesParties = Connection::executerSelect($requeteGetParties,[":ID"=>$idJoueur]);
				
				//On cre le tableau a renvoyer
				$response["id"]     = $idJoueur;
				$response["gagner"] 	= $nbGagner;
				$response["perdu"]  	= $nbPerdu;
				$response["null"] 		= $nbNull;
				$response["nbNivCree"] 	= $nbNiveauCreer;
				$response["partiesJouees"] = $nbPartieJouer;
				$response["ratioGagner"]  = $ratioGagner;
				$response["ratioPerdu"]	   = $ratioPerdu;
				$response["ratioAbandon"]  = $ratioAbandon;
				$response["reputation"]    = $reputation;
				$response["armeFav"]	   = $armeFav;
				$response["armeDegat"]     = $armeDegat[0];
				$response["parties"]       = $tousLesParties;
				$response["dmgMoyen"]	   = DAOStatistique::getDommageMoyen($armeDegat);
				$response["dmgMoyenTir"]	   = DAOStatistique::getDommageMoyenParTir($armeDegat);


			}
			return $response;
		}
		
		static private function getDommageMoyen($donnee)
		{
			$arrArme = array();
			$totDmg = 0;
			$totTire = 0;
			$index = 0;
			foreach($donnee as $row)
			{
				$totDmg = $row["QTE_DOMMAGE_TOTAL"];
				$totTire = $row["NB_FOIS_TIRE"];
				$arrArme[$index][0] = $row["NOM"];

				if($totTire==0)
					$arrArme[$index][1] = $totDmg;
				else
					$arrArme[$index][1] = $totDmg/$totTire;

				if(empty($arrArme[$index][1]))
					$arrArme[$index][1] = 0;
	
				$index += 1;
			}

			return $arrArme;
		}

		static private function getDommageMoyenParTir($donnee)
		{
			$totDmg = 0;
			$totTire = 0;
			$index = 0;
			foreach($donnee as $row)
			{
				$totDmg += $row["QTE_DOMMAGE_TOTAL"];
				$totTire += $row["NB_FOIS_TIRE"];
				$index += 1;
			}
			if($totTire!=0)
				$res= $totDmg/$totTire;
			else
				$res=0;
			return round($res,2);
		}

		/*//on le fait déja dans le PLSQL
		static private function genererQualifA($nbGagner,$armeDegat)
		{
			$qualifA = "";
			$armeLePlusTire = DAOStatistique::getArmeLePlusTire($armeDegat);

			if($nbGagner > 2)
				$qualifA = "Le généraliste";
			else if($armeLePlusTire == "Piege")
				$qualifA = "Le subtil";
			else if($armeLePlusTire == "Missile")
				$qualifA = "Le subtil";
			else if($armeLePlusTire == "Mitraillette")
				$qualifA = "Le subtil";
			else if($armeLePlusTire == "Grenade")
				$qualifA = "Le subtil";
			else if($armeLePlusTire == "Shotgun")
				$qualifA = "Le subtil";
			else if($armeLePlusTire == "Spring")
				$qualifA = "Le subtil";

			return $qualifA;
		}

		static private function genererQualifB()
		{
			return;
		}

		static private function getArmeLePlusTire($tab)
		{
			foreach($tab as $row)
			{
				if($row["ARME.NOM"] == $nomArme)
					return $row;
			}

			return -1;
		}
		*/
	}
?>