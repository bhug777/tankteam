DROP SEQUENCE ma_seq_spawn;

create sequence ma_seq_spawn
start with 1 
increment by 1 
nomaxvalue; 

CREATE TABLE spawn(
	id 					INTEGER DEFAULT seq_spawn.NEXTVAL PRIMARY KEY,
	idniveau 			INTEGER 		NOT NULL,
	posx 				INTEGER 		NOT NULL,
	posy 				INTEGER 		NOT NULL,
	CONSTRAINT idspawn_fk FOREIGN KEY(idniveau) REFERENCES mapTT(id) ON DELETE CASCADE
);
commit;