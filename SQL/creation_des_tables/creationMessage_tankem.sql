DROP SEQUENCE ma_seq_messages;

create sequence ma_seq_messages 
start with 1 
increment by 1 
nomaxvalue; 

CREATE TABLE message_tankem(
message_accueil VARCHAR2(60),
message_debut VARCHAR2(50),
message_fin VARCHAR2(70),
id_mess INTEGER DEFAULT ma_seq_messages.NEXTVAL PRIMARY KEY
);
commit;