DROP SEQUENCE ma_seq_map;
create sequence ma_seq_map 
start with 1 
increment by 1 
nomaxvalue; 
CREATE TABLE mapTT(
	id 					INTEGER DEFAULT ma_seq_map.NEXTVAL PRIMARY KEY,
	nbX 				INTEGER NOT NULL,
	nbY 				INTEGER NOT NULL,
	etat 				VARCHAR(10) NOT NULL,/*Public, prive...*/
	nom 				VARCHAR(20) UNIQUE NOT NULL,
	tempsMin 			INTEGER NOT NULL,
	tempsMax 			INTEGER NOT NULL
);
commit;