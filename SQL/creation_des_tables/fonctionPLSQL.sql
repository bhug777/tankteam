BEGIN
IF (in_config1 IS NOT NULL OR in_config1 != '') THEN
  UPDATE question_table
	 SET comment = in_config1
   WHERE id = id
	 AND questionid = 1;
  if sql%notfound then
	INSERT INTO question_table (tid, questionid, comments) VALUES (id, 1, in_config1);
  end if;
END IF;
exception
when others then
  dbms_output.put_line(sqlerrm);
END;



create or replace PROCEDURE get_reputation(V_REPUTATION IN VARCHAR2) IS 
DECLARE
 V_REPUTATION UTILISATEUR.REPUTATION%TYPE;
 --V_NB_PARTIE_JOUE UTILISATEUR.NB_PARTIE_JOUE%TYPE;
 --V_NB_PARTIE_GAGNE UTILISATEUR.NB_PARTIE_GAGNE%TYPE;
 --V_NB_PARTIE_PERDU UTILISATEUR.NB_PARTIE_PERDU%TYPE;
 --V_NB_PARTIE_ABANDON UTILISATEUR.NB_PARTIE_ABANDON%TYPE;
BEGIN
 SELECT ID INTO V_ID FROM UTILISATEUR;
 SELECT NB_PARTIE_JOUE INTO V_NB_PARTIE_JOUE FROM UTILISATEUR;
 SELECT NB_PARTIE_GAGNE INTO V_NB_PARTIE_GAGNE FROM UTILISATEUR;
 SELECT NB_PARTIE_PERDU INTO V_NB_PARTIE_PERDU FROM UTILISATEUR;
 SELECT NB_PARTIE_ABANDON INTO V_NB_PARTIE_ABANDON FROM UTILISATEUR;
 SELECT NB_PARTIE_ABANDON INTO V_NB_PARTIE_ABANDON FROM UTILISATEUR;
 
 
--QUALIFICATIF A: 
IF V_NB_PARTIE_GAGNE > 1 THEN
	V_REPUTATION:='GÉNÉRALISTE';
ELSIF  V_NB_PARTIE_GAGNE < 1 THEN
	V_REPUTATION:='AAAGÉNÉRALISTE';
	
END get_reputation;
	


	CREATE OR REPLACE TRIGGER TRG
  AFTER INSERT
  ON PARTIE
  
DECLARE
  V_ID INTEGER;
  V_NB_PARTIE_JOUE INTEGER;
  V_NB_PARTIE_GAGNE INTEGER;
  V_NB_PARTIE_PERDU INTEGER;
  V_NB_PARTIE_ABANDON INTEGER;

BEGIN
 SELECT ID INTO V_ID1 FROM UTILISATEUR WHERE ID=(SELECT ID_J1 FROM PARTIE);
 SELECT ID INTO V_ID2 FROM UTILISATEUR WHERE ID=(SELECT ID_J2 FROM PARTIE);
 SELECT PARTIE_NUL INTO V_PARTIE_NUL FROM PARTIE;
 SELECT ID_GAGNANT INTO V_GAGNANT FROM UTILISATEUR; 
 
 
 --SELECT NB_PARTIE_JOUE INTO V_NB_PARTIE_JOUE FROM UTILISATEUR WHERE ID=5;
 --SELECT NB_PARTIE_GAGNE INTO V_NB_PARTIE_GAGNE FROM UTILISATEUR WHERE ID=5;
 --SELECT NB_PARTIE_PERDU INTO V_NB_PARTIE_PERDU FROM UTILISATEUR WHERE ID=5;
 --SELECT NB_PARTIE_ABANDON INTO V_NB_PARTIE_ABANDON FROM UTILISATEUR WHERE ID=5;
 
 
--QUALIFICATIF A: 
IF V_NB_PARTIE_GAGNE > 1 THEN
  UPDATE UTILISATEUR SET REPUTATION = 'GENERALISTE' WHERE ID=5;
ELSIF  V_NB_PARTIE_GAGNE < 1 THEN
  UPDATE UTILISATEUR SET REPUTATION = 'AAAAGENERALISTE' WHERE ID=5;
END IF;
END;	
        