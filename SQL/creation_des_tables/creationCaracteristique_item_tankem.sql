CREATE TABLE caracteristique_item_tankem (
id VARCHAR2(10) PRIMARY KEY,
val_max FLOAT(4),
val_min FLOAT(4),
defaut FLOAT(4),
valeur FLOAT(4),
nom VARCHAR2(50)
);
commit;
