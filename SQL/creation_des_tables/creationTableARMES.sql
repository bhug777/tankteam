DROP SEQUENCE ma_seq_armes;

create sequence ma_seq_armes 
start with 1 
increment by 1 
nomaxvalue; 

CREATE TABLE ARMES(
ID_ARME 			INTEGER DEFAULT ma_seq_armes.NEXTVAL PRIMARY KEY,
NOM 				VARCHAR2(50),
DESCRIPTION			VARCHAR2(150)
);
commit;