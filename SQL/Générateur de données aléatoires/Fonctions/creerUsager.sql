create or replace PROCEDURE creerUsager(v_nom IN VARCHAR2) IS
V_NOM1 VARCHAR2(50);
V_PRENOM1 VARCHAR2(50);
V_QUESTION_A VARCHAR2(100);
V_QUESTION_B VARCHAR2(100);
V_REPONSE_A VARCHAR2(100);
V_REPONSE_B VARCHAR2(100);
V_COULEUR NUMBER;
BEGIN
	select dbms_random.string('u', 10) INTO V_NOM1 from dual;
	select dbms_random.string('u', 10) INTO V_PRENOM1 from dual;
	select dbms_random.string('u', 10) INTO V_QUESTION_A from dual;
	select dbms_random.string('u', 10) INTO V_QUESTION_B from dual;
	select dbms_random.string('u', 10) INTO V_REPONSE_A from dual;
	select dbms_random.string('u', 10) INTO V_REPONSE_B from dual;
	V_COULEUR := 255255255255;
	--select dbms_random.value('u', 10) INTO V_COULEUR from dual;
	INSERT INTO UTILISATEUR(NOM_UTILISATEUR,MOT_DE_PASSE,NOM,PRENOM,QUESTION_A,QUESTION_B,REPONSE_A,REPONSE_B,COULEUR) VALUES (v_nom,'$2y$10$bF3JSEQd05PYsuG9JS0Cee0RysuTz/ZSeqWbhSXyPFoWcKlnXdjCO',V_NOM1,V_PRENOM1,V_QUESTION_A,V_QUESTION_B,V_REPONSE_A,V_REPONSE_B,V_COULEUR);
	
END;