create or replace PROCEDURE creerNiveau(v_nom IN VARCHAR2) IS
V_NBX NUMBER;
V_NBY NUMBER;
V_ETAT VARCHAR2(10);
V_NOM1 VARCHAR2(20);
V_TEMPSMIN NUMBER;
V_TEMPSMAX NUMBER;
V_ VARCHAR2(20);
V_ID NUMBER;
V_RAND NUMBER;
V_k NUMBER;
V_RAND2 NUMBER;

BEGIN
    V_RAND := mod(abs(dbms_random.random),2);
    IF V_RAND = 1 THEN
            V_ETAT := 'Prive';
    ELSE
            V_ETAT := 'Public';
    END IF;
    
    select dbms_random.string('u', 10) INTO V_NOM1 from dual;
    V_NBX := mod(abs(dbms_random.random),6)+6;
    V_NBY := V_NBX;
    --V_NBY := mod(abs(dbms_random.random),6)+6;
    V_TEMPSMIN := mod(abs(dbms_random.random),1)+1;
    V_TEMPSMAX := mod(abs(dbms_random.random),1)+2;

    
    INSERT INTO MAPTT(NBX,NBY,ETAT,NOM,TEMPSMIN,TEMPSMAX) VALUES (V_NBX,V_NBY,V_ETAT,V_NOM1, V_TEMPSMIN,V_TEMPSMAX);
    SELECT MAX(ID) INTO V_ID FROM MAPTT;
    SELECT ID INTO V_ FROM UTILISATEUR WHERE NOM_UTILISATEUR =(V_NOM);
    INSERT INTO NIVEAUXCREATION(ID_USER,ID_NIVEAU)VALUES(V_,V_ID);
    V_k :=0;
    
     
    
    FOR i IN 0..V_NBX LOOP
       FOR j IN 0..V_NBY LOOP
          V_RAND2 := mod(abs(dbms_random.random),4);
          INSERT INTO VALEURCASE(ID,ID_MAPTT,POSX,POSY,VALEUR)VALUES(V_k,V_ID,i,j,V_RAND2);
          --INSERT INTO SPAWN(ID,IDNIVEAU,POSX,POSY)VALUES(V_k,V_ID,i,j);
          V_k := V_k +1;
      END LOOP;
    END LOOP;
    
END;