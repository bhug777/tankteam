# -*- coding: utf-8 -*-
#import  dependencies
from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
#Module de Panda3D
from direct.showbase.ShowBase import ShowBase

#Modules internes
from Internal.DTO.dtoMap import DTOMap
from functools import partial
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
import sys
from Internal.DTO.dtoMap import DTOMap
from Internal.DAO.DAOMap import DAO_Map
from Internal.DAO.DAOUtilisateur import DAOUtilisateur
from subprocess import *

class InterfaceMenuChoixMap(ShowBase):
    def __init__(self,joueur1,joueur2,listeArmeJ1,listeArmeJ2,sj1,sj2):
        #Image d'arrière plan
        self.j1 = joueur1
        self.j2 = joueur2
        self.laj1 = listeArmeJ1
        self.laj2 = listeArmeJ2
        self.sj1=sj1
        self.sj2=sj2
        self.background=OnscreenImage(parent=render2d, image="../asset/Menu/menubg.jpg")

        #On dit à la caméra que le dernier modèle doit s'afficher toujours en arrière
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(20)

        #import des maps
        self.liste_de_dict_map = []

        self.dto= DAO_Map().lireDB()

        self.liste_de_dict_map=self.dto.getTousLesMaps()

        self.listeParam =["nom","nbCol","nbRow","etat","tempsMin","tempsMax"]
        self.bk_titre = "Faites votre choix de niveau:"
        self.textObject = OnscreenText(text = self.bk_titre,
                                    pos = (0.95,0.7), #Position
                                    scale = 0.1, #scale
                                    fg=(0,0,0,0), #couleur du texte
                                    align=TextNode.ACenter, #alignement
                                    mayChange=0) #est ce que le texte peut changer?

        self.bk_text = "Version 0.0.0.1"
        self.textObject = OnscreenText(text = self.bk_text,
                                    pos = (-0.95,-0.95), #Position
                                    scale = 0.07, #scale
                                    fg=(1,0.5,0.5,1), #couleur du texte
                                    align=TextNode.ACenter, #alignement
                                    mayChange=1) #est ce que le texte peut changer?

        self.b = DirectButton(text = (u"Lancer jeu", "click!", "yolo", "disabled"),
                        scale=.1,
                        command=self.chargeJeu)
        self.b.setPos(0.1,-0.5,-0.5)
        numItemsVisible = 4
        itemHeight = 0.11

        self.myScrolledList = DirectScrolledList(
            decButton_pos= (0.35, 0, 0.53),
            decButton_text = "-",
            decButton_text_scale = 0.2,
            decButton_borderWidth = (0.005, 0.005),#Btn pour derouler liste
         
            incButton_pos= (0.35, 0, -0.08),
            incButton_text = "+",
            incButton_text_scale = 0.2,
            incButton_borderWidth = (0.005, 0.005),
         
            frameSize = (1, 1, -0.05, 0.8),
            frameColor = (1,1,1,0.5),
            pos = (0.7, 0.7, 0),
            #items = [b1, b2], #liste item par defaut
            numItemsVisible = numItemsVisible,
            forceHeight = itemHeight, #item trop petit aura une hauteur mini
            itemFrame_frameSize = (-0.3, 0.3, -0.37, 0.11),
            itemFrame_pos = (0.35, 0, 0.4),
            )


        for carte in self.liste_de_dict_map:
            l = DirectButton(text = (str(carte["nom"]), str(carte["nom"]), ">"+str(carte["nom"])+"<", "disabled"),
                          text_scale=0.1, borderWidth = (0.01, 0.01),
                          relief=0, command=partial(self.setText,carte))
            self.myScrolledList.addItem(l)

        #Initialisation de l'effet de transition
        curtain = loader.loadTexture("../asset/Menu/loading.jpg")

        self.transition = Transitions(loader)
        self.transition.setFadeColor(0, 0, 0)
        self.transition.setFadeModel(curtain)

        self.sound = loader.loadSfx("../asset/Menu/demarrage.mp3")
        self.zoneText=[]
        self.zoneText1=[]
        self.zoneText2=[]
        incr=0.8
        for param in self.listeParam:
            texte = ""
            self.zoneText.append(OnscreenText(text = texte,
                                pos = (-0.95,incr), #Position
                                scale = 0.07, #scale
                                fg=(1,0.5,0.5,1), #couleur du texte
                                align=TextNode.ACenter, #alignement
                                mayChange=1)) #est ce que le texte peut changer?) 
            incr-=0.1
      
        self.zoneText1.append(OnscreenText(text = texte,
                            pos = (-0.95,incr), #Position
                            scale = 0.07, #scale
                            fg=(1,0.5,0.5,1), #couleur du texte
                            align=TextNode.ACenter, #alignement
                            mayChange=1)) #est ce que le texte peut changer?) 
        incr-=0.1
        self.zoneText2.append(OnscreenText(text = texte,
                            pos = (-0.95,incr), #Position
                            scale = 0.07, #scale
                            fg=(1,0.5,0.5,1), #couleur du texte
                            align=TextNode.ACenter, #alignement
                            mayChange=1)) #est ce que le texte peut changer?) 
        incr-=0.1
        # self.zoneText.append(OnscreenText(text = texte,
        #                         pos = (-0.95,incr), #Position
        #                         scale = 0.07, #scale
        #                         fg=(1,0.5,0.5,1), #couleur du texte
        #                         align=TextNode.ACenter, #alignement
        #                         mayChange=1))
        # for label in self.listeParam1:
        #     label.setText("Createur: "+ str((self.nomProprio[0])[0]))

    def setText(self, carte):
        for label,param in zip(self.zoneText,self.listeParam):
            label.setText(param+" : "+ str(carte[param]))
            if param=="nom":

                self.id_niveau = DAOUtilisateur().selectIDUSERF(str(carte[param]))
       
                self.id_user = DAOUtilisateur().selectIDUSER((self.id_niveau[0])[0])


                self.nb = DAOUtilisateur().selectIDUSER1((self.id_niveau[0])[0])
       
                #print str(self.id_user)
                self.nomProprio = DAOUtilisateur().selectNOMUSER((self.id_user[0])[0])
    
                self.a = ((self.nomProprio[0])[0])
            

        for label,param in zip(self.zoneText1,self.listeParam):
            label.setText("Createur : " + self.a)

        for label,param in zip(self.zoneText2,self.listeParam):
            label.setText("Ajout au favoris : " + str((self.nb[0])[0]))

        self.carteChoisie = carte

    def cacher(self):
        #Est esssentiellement un code de "loading"

        #On remet la caméra comme avant
        base.cam.node().getDisplayRegion(0).setSort(self.baseSort)
        #On cache les menus
        self.background.hide()
        self.myScrolledList.hide()
        self.b.hide()
        for fobject in self.zoneText:
            fobject.hide()
        for fobject in self.zoneText1:
            fobject.hide()
        for fobject in self.zoneText2:
            fobject.hide()

    def chargeJeu(self):
        #On démarre!
        Sequence(Func(lambda : self.transition.irisOut(0.2)),
                 SoundInterval(self.sound),
                 Func(self.cacher),
                 Func(lambda : messenger.send("DemarrerPartie",sentArgs=[self.carteChoisie,self.j1,self.j2,self.laj1,self.laj2,self.sj1,self.sj2])),
                 Wait(0.2), #Bug étrange quand on met pas ça. L'effet de transition doit lagger
                 Func(lambda : self.transition.irisIn(0.2))
        ).start()
