## -*- coding: utf-8 -*-

from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
from interfaceMenuChoixMap import InterfaceMenuChoixMap
from panda3d.bullet import *
from panda3d.core import *
import webbrowser
import math

from Internal.DAO.DAOUtilisateur import DAOUtilisateur

from direct.actor.Actor import Actor

import sys

class InterfaceMenuAuthentification(ShowBase):
    def __init__(self):
        self.connecter=0
        self.j1_armeP=""
        self.j2_armeP=""
        self.j1_armeS=""
        self.j2_armeS=""
        self.background=OnscreenImage(parent=render2d, image="../asset/Menu/menubg.jpg")
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(20)

        self.j1 = OnscreenText(text = "Joueur 1",
                                      pos = (-0.99,0.8),
                                      scale = 0.10,
                                      fg=(0,0,0,1),
                                      align=TextNode.ARight)

        self.j1mdp = OnscreenText(text = "Mot de passe",
                                      pos = (-0.99,0.6),
                                      scale = 0.10,
                                      fg=(0,0,0,1),
                                      align=TextNode.ARight)

        self.j2 = OnscreenText(text = "Joueur 2",
                                      pos = (0.3,0.8),
                                      scale = 0.10,
                                      fg=(0,0,0,1),
                                      align=TextNode.ALeft)

        self.j2mdp = OnscreenText(text = "Mot de passe",
                                      pos = (0.3,0.6),
                                      scale = 0.10,
                                      fg=(0,0,0,1),
                                      align=TextNode.ALeft)
         
        self.j1Nom = DirectEntry(text = "" ,
                        pos = (-0.8,0,0.8),
                        scale=0.07,
                        initialText="ALUT88",
                        numLines = 1,
                        focus=1)

        self.j1mdpE = DirectEntry(text = "" ,
                        pos = (-0.8,0,0.6),
                        scale=0.07,
                        obscured=True,
                        command=self.connexion,
                        initialText="AAAaaa111**",
                        numLines = 1)

        self.j2Nom = DirectEntry(text = "" ,
                        pos = (0.8,0,0.8),
                        scale=0.07,
                        initialText="ALUT",
                        numLines = 1)

        self.j2mdpE = DirectEntry(text = "" ,
                        pos = (0.95,0,0.6),
                        scale=0.07,
                        obscured=True,
                        command=self.connexion,
                        initialText="AAAaaa111**",
                        numLines = 1)

        self.textObject = OnscreenText(text = "test de connection", 
                              pos = (0,-0.95),
                              scale = 0.10,
                              align=TextNode.ACenter,
                              mayChange=1)

        self.b2 = DirectButton(text=("page Web","page Web","page Web","disabled"),
                                text_scale=0.07,
                                borderWidth=(0.02,0.02),
                                relief=2,
                                pos=(.9,0,-0.9),
                                command=self.site)

    def site(self):
      url = 'http://www.python.org/'
      webbrowser.open_new_tab(url + 'doc/')

    def setTexte(self,textEntered):
        self.textObject.setText(textEntered)

    def clearText():
        b.enterText('')

    def connexion(self,text):
      if(self.j1Nom.get(plain=True) and self.j1mdpE.get(plain=True)):
        self.nom=self.j1Nom.get(plain=True)
        self.mdp=self.j1mdpE.get(plain=True)
        self.j1mdpE.enterText('')
        self.connect(self.nom,self.mdp,0)
      elif(self.j2Nom.get(plain=True) and self.j2mdpE.get(plain=True)):
        self.nom=self.j2Nom.get(plain=True)
        self.mdp=self.j2mdpE.get(plain=True)
        self.j2mdpE.enterText('')
        self.connect(self.nom,self.mdp,1)
      else:
        self.setTexte("donner manquante")

    def connect(self,nom,mdp,no):
      #on va tanter de ce connecter avec le nom et mdp passe
      #si la connection est faite l'objet joueur va etre creer pour le j specifique
      #on va effacer je qui etait visible pour lui 
      #passer un message d'erreur si ca ne marche pas
      #faire un check si les deux son connecter pour faire self.joueursConnecter()
      if(no==0):
        self.joueur1=DAOUtilisateur().testerConnexion(nom,mdp) #DAOUtilisateur.......
        if(self.joueur1!=None):
          self.authentifier_j1(self.joueur1)
          self.connecter+=1
        else:
          self.setTexte("connexion échoué j1")
      elif(no==1):
        self.joueur2=DAOUtilisateur().testerConnexion(nom,mdp)
        if(self.joueur2!=None):
          self.authentifier_j2(self.joueur2)
          self.connecter+=1
        else:
          self.setTexte("connexion échoué j2")
      if(self.connecter==2):
        self.joueursConnecter()

    def decoCouleur(self,c):
      rouge = float((c/1000000000)/255.0)
      vert = float(((c/1000000)%1000)/255.0)
      bleu = float(((c/1000)%1000)/255.0)
      return (rouge,vert,bleu,1)

    def authentifier_j1(self,joueur):
      #appeler la fct corespondante au bon joueur pour cacher les info
      self.cacherj1()
      v=[0]
      print
      self.cj1=self.decoCouleur(joueur.getTousLesUtilisateurs()[0]["COULEUR"])

      #print str(joueur.getTousLesUtilisateurs()[0]["MAPS"])

      #print "'Map joueur1 "+ str(joueur.getTousLesUtilisateurs()[0]["MAPS"])

      #print joueur.getTousLesUtilisateurs()[0]["MAPS"]

      numItemsVisible = 7
      itemHeight = 0.10

      self.j1_nomArme = OnscreenText(text = "arme",
                                      pos = (-.99,0.9), 
                                      scale = 0.05,
                                      fg=(0,0,0,1),
                                      align=TextNode.ARight)

      self.j1_myScrolledList = DirectScrolledList(
          decButton_pos= (1, 0, 1),
          decButton_text = "",
          decButton_text_scale = 0.2,
          decButton_borderWidth = (0.005, 0.005),
        
          incButton_pos= (1, 0, 1),
          incButton_text = "",
          incButton_text_scale = 0.2,
          incButton_borderWidth = (0.005, 0.005),
         
          frameSize = (1, 1, -0.05, 0.7),
          frameColor = (1,0,0,0),
          color=(1,1,1,0),
          pos = (-0.5, 0.7, 0),
          numItemsVisible = numItemsVisible,
          forceHeight = itemHeight,
          itemFrame_frameSize = (-0.3, 0.3, -0.7, 0.11),
          itemFrame_pos = (-0.6, 0, 0.75),
          )

      self.j1_primaire = OnscreenText(text = "Primaire",
                                      pos = (-.6,0.9), 
                                      scale = 0.05,
                                      fg=(0,0,0,1),
                                      align=TextNode.ARight)

      self.j1_myScrolledList1 = DirectScrolledList(
          decButton_pos= (1, 0, 1),
          decButton_text = "",
          decButton_text_scale = 0.2,
          decButton_borderWidth = (0.005, 0.005),
        
          incButton_pos= (1, 0, 1),
          incButton_text = "",
          incButton_text_scale = 0.2,
          incButton_borderWidth = (0.005, 0.005),
         
          frameSize = (.5, .5, -0.05, 0.7),
          frameColor = (1,1,1,0.5),
          pos = (-0.7, 0.7, 0),
          numItemsVisible = numItemsVisible,
          forceHeight = itemHeight,
          itemFrame_frameSize = (-0.1, 0.05, -0.7, 0.11),
          itemFrame_pos = (.05, 0, 0.75),
          )

      self.j1_secondaire = OnscreenText(text = "Secondaire",
                                      pos = (-.3,0.9), 
                                      scale = 0.05,
                                      fg=(0,0,0,1),
                                      align=TextNode.ARight)

      self.j1_myScrolledList2 = DirectScrolledList(
          decButton_pos= (1, 0, 1),
          decButton_text = "",
          decButton_text_scale = 0.2,
          decButton_borderWidth = (0.005, 0.005),
        
          incButton_pos= (1, 0, 1),
          incButton_text = "",
          incButton_text_scale = 0.2,
          incButton_borderWidth = (0.005, 0.005),
         
          frameSize = (.5, .5, -0.05, 0.7),
          frameColor = (1,1,1,0.5),
          pos = (-0.8, 0.7, 0),
          numItemsVisible = numItemsVisible,
          forceHeight = itemHeight,
          itemFrame_frameSize = (-0.1, 0.05, -0.7, 0.11),
          itemFrame_pos = (.35, 0, 0.75),
          )

      b=[0]
      self.j1_liste=[]
      self.j1_liste2=[]
      self.j1_myScrolledList.addItem(DirectLabel(text = "Canon", text_scale=0.08))
      l = DirectRadioButton(text = "", scale=0.05, pos=(-0.4,0,0), command=self.j1p ,extraArgs=["Canon"])
      l.setIndicatorValue
      self.j1_liste.append(l)
      self.j1_myScrolledList1.addItem(l)
      #l2 = DirectRadioButton(text = "",indicatorValue=True, variable=v, value=[0], scale=0.05, pos=(-0.4,0,0),command=self.j1s,extraArgs="Canon")
      l2 = DirectRadioButton(text = "", scale=0.05, pos=(-0.4,0,0),command=self.j1s,extraArgs=["Canon"])
      self.j1_liste2.append(l2)
      self.j1_myScrolledList2.addItem(l2)

      for arme in joueur.getTousLesUtilisateurs()[0]["EQUIPEMENT"]:
        l3 = DirectLabel(text = arme + " " + str(joueur.getTousLesUtilisateurs()[0]["EQUIPEMENT"][arme]["QTE"]), text_scale=0.08)
        l = DirectRadioButton(text = "",scale=0.05, pos=(-0.4,0,0),command=self.j1p,extraArgs=[arme])
        l2 = DirectRadioButton(text = "", scale=0.05, pos=(-0.4,0,0),command=self.j1s,extraArgs=[arme])
        self.j1_liste.append(l)
        self.j1_liste2.append(l2)
        self.j1_myScrolledList.addItem(l3)
        self.j1_myScrolledList1.addItem(l)
        self.j1_myScrolledList2.addItem(l2)
      for button in self.j1_liste:
        button.setOthers(self.j1_liste)
      for button in self.j1_liste2:
        button.setOthers(self.j1_liste2)

      base.cam.setPos(2.03,-8.81,1.27)
      base.cam.setHpr(0.0,0,0)
      self.modele1 = base.loader.loadModel("../asset/Tank/tank")
      self.modele1.setScale(0.75,0.75,0.75)
      self.modele1.setPos(-4.30,1.83,-0.16)
      self.modele1.setHpr(213.02,0,0)
      self.modele1.setColorScale(self.cj1)
      self.modele1.reparentTo(base.render)
      rotation=self.modele1.hprInterval(50, Vec3(10, 0, 0))
      seq=self.modele1.posInterval(2, Point3(-0.30,1.83,-0.16))
      myParallel = Parallel(rotation)
      mySequence = Sequence(seq)
      myParallel.loop()
      mySequence.start()

    def authentifier_j2(self,joueur):
      self.cacherj2()
      v=[0]

      numItemsVisible = 7
      itemHeight = 0.10

      self.cj2=self.decoCouleur(joueur.getTousLesUtilisateurs()[0]["COULEUR"])

      self.j2_nomArme = OnscreenText(text = "arme",
                                      pos = (.53,0.9), 
                                      scale = 0.05,
                                      fg=(0,0,0,1),
                                      align=TextNode.ARight)

      self.j2_myScrolledList = DirectScrolledList(
          decButton_pos= (1, 0, 1),
          decButton_text = "",
          decButton_text_scale = 0.2,
          decButton_borderWidth = (0.005, 0.005),
        
          incButton_pos= (1, 0, 1),
          incButton_text = "",
          incButton_text_scale = 0.2,
          incButton_borderWidth = (0.005, 0.005),
         
          frameSize = (1, 1, -0.05, 0.7),
          frameColor = (1,0,0,0),
          color=(1,1,1,0),
          pos = (-0.1, 0.7, 0),
          numItemsVisible = numItemsVisible,
          forceHeight = itemHeight,
          itemFrame_frameSize = (-0.3, 0.3, -0.7, 0.11),
          itemFrame_pos = (0.6, 0, 0.75),
          )

      self.j2_primaire = OnscreenText(text = "Primaire",
                                      pos = (.95,0.9), 
                                      scale = 0.05,
                                      fg=(0,0,0,1),
                                      align=TextNode.ARight)

      self.j2_myScrolledList1 = DirectScrolledList(
          decButton_pos= (1, 0, 1),
          decButton_text = "",
          decButton_text_scale = 0.2,
          decButton_borderWidth = (0.005, 0.005),
        
          incButton_pos= (1, 0, 1),
          incButton_text = "",
          incButton_text_scale = 0.2,
          incButton_borderWidth = (0.005, 0.005),
         
          frameSize = (.6, .5, -0.05, 0.7),
          frameColor = (1,1,1,0.5),
          pos = (0.88, 0.7, 0),
          numItemsVisible = numItemsVisible,
          forceHeight = itemHeight,
          itemFrame_frameSize = (-0.1, 0.05, -0.7, 0.11),
          itemFrame_pos = (.05, 0, 0.75),
          )

      self.j2_secondaire = OnscreenText(text = "Secondaire",
                                      pos = (.99,0.9), 
                                      scale = 0.05,
                                      fg=(0,0,0,1),
                                      align=TextNode.ALeft)

      self.j2_myScrolledList2 = DirectScrolledList(
          decButton_pos= (1, 0, 1),
          decButton_text = "",
          decButton_text_scale = 0.2,
          decButton_borderWidth = (0.005, 0.005),
        
          incButton_pos= (1, 0, 1),
          incButton_text = "",
          incButton_text_scale = 0.2,
          incButton_borderWidth = (0.005, 0.005),
         
          frameSize = (.5, .5, -0.05, 0.7),
          frameColor = (1,1,1,0.5),
          pos = (0.8, 0.7, 0),
          numItemsVisible = numItemsVisible,
          forceHeight = itemHeight,
          itemFrame_frameSize = (-0.1, 0.05, -0.7, 0.11),
          itemFrame_pos = (.35, 0, 0.75),
          )

      b=[0]
      self.liste=[]
      self.liste2=[]

      self.j2_myScrolledList.addItem(DirectLabel(text = "Canon", text_scale=0.08))
      l = DirectRadioButton(text = "",scale=0.05, pos=(-0.4,0,0),command=self.j2p,extraArgs=["Canon"])
      self.liste.append(l)
      self.j2_myScrolledList1.addItem(l)
      l2 = DirectRadioButton(text = "", scale=0.05, pos=(-0.4,0,0),command=self.j2s,extraArgs=["Canon"])
      self.liste2.append(l2)
      self.j2_myScrolledList2.addItem(l2)

      for arme in joueur.getTousLesUtilisateurs()[0]["EQUIPEMENT"]:
        l3 = DirectLabel(text = arme + " " + str(joueur.getTousLesUtilisateurs()[0]["EQUIPEMENT"][arme]["QTE"]), text_scale=0.08)
        l = DirectRadioButton(text = "",scale=0.05, pos=(-0.4,0,0),command=self.j2p,extraArgs=[arme])
        l2 = DirectRadioButton(text = "",scale=0.05, pos=(-0.4,0,0),command=self.j2s,extraArgs=[arme])
        self.liste.append(l)
        self.liste2.append(l2)
        self.j2_myScrolledList.addItem(l3)
        self.j2_myScrolledList1.addItem(l)
        self.j2_myScrolledList2.addItem(l2)
      for button in self.liste:
        button.setOthers(self.liste)
      for button in self.liste2:
        button.setOthers(self.liste2)

      base.cam.setPos(2.03,-8.81,1.27)
      base.cam.setHpr(0.0,0,0)
      self.modele2 = base.loader.loadModel("../asset/Tank/tank")
      self.modele2.setScale(0.75,0.75,0.75)
      self.modele2.setPos(8.32,1.83,-0.16)
      self.modele2.setHpr(213.02,0,0)
      self.modele2.setColorScale(self.cj2)
      self.modele2.reparentTo(base.render)
      rotation=self.modele2.hprInterval(50, Vec3(10, 0, 0))
      seq=self.modele2.posInterval(2, Point3(4.80,1.83,-0.16))
      myParallel = Parallel(rotation)
      mySequence = Sequence(seq)
      myParallel.loop()
      mySequence.start()

    def joueursConnecter(self):
      self.textObject.hide()
      self.b1 = DirectButton(text = ("Combattre", "Combattre", "Combattre", "disabled"),
                          text_scale=0.08,
                          borderWidth = (0.04,0.04),
                          text_bg=(0.243,0.325,0.121,1),
                          frameColor=(0.243,0.325,0.121,1),
                          relief=2,
                          command=self.chargeMenuChoixNiveau,
                          pos = (0,0,-0.85))

    def cacherj1(self):
      self.j1.hide()
      self.j1Nom.hide()
      self.j1mdp.hide()
      self.j1mdpE.hide()

    def cacherj2(self):
      self.j2.hide()
      self.j2Nom.hide()
      self.j2mdp.hide()
      self.j2mdpE.hide()

    def cacher(self):
        base.cam.node().getDisplayRegion(0).setSort(self.baseSort)
        self.b2.hide()
        self.modele1.hide()
        self.modele2.hide()
        self.background.hide()
        self.j1_myScrolledList.hide()
        self.j1_myScrolledList1.hide()
        self.j1_myScrolledList2.hide()
        self.j1_nomArme.hide()
        self.j1_primaire.hide()
        self.j1_secondaire.hide()
        self.j2_myScrolledList.hide()
        self.j2_myScrolledList1.hide()
        self.j2_myScrolledList2.hide()
        self.j2_nomArme.hide()
        self.j2_primaire.hide()
        self.j2_secondaire.hide()
        self.b1.hide()

    def j1p(self,nom):
      self.j1_armeP=nom

    def j1s(self,nom):
      self.j1_armeS=nom

    def j2p(self,nom):
      self.j2_armeP=nom

    def j2s(self,nom):
      self.j2_armeS=nom

    def chargeMenuChoixNiveau(self):
        self.cacher()
        listeArmeJ1=[self.j1_armeP,self.j1_armeS]
        listeArmeJ2=[self.j2_armeP,self.j2_armeS]
        listeRetirerArme=[]
        for arme in listeArmeJ1:
          if(arme!="Canon"):
            idArme = self.joueur1.getTousLesUtilisateurs()[0]["EQUIPEMENT"][arme]["ARME"]["ID"]
            idUtil = self.joueur1.getTousLesUtilisateurs()[0]["ID"]
            qte = self.joueur1.getTousLesUtilisateurs()[0]["EQUIPEMENT"][arme]["QTE"]
            listeRetirerArme.append([idArme,idUtil,qte])

        for arme in listeArmeJ2:
          if(arme!="Canon"):
            idArme = self.joueur2.getTousLesUtilisateurs()[0]["EQUIPEMENT"][arme]["ARME"]["ID"]
            idUtil = self.joueur2.getTousLesUtilisateurs()[0]["ID"]
            qte = self.joueur2.getTousLesUtilisateurs()[0]["EQUIPEMENT"][arme]["QTE"]
            listeRetirerArme.append([idArme,idUtil,qte])

        DAOUtilisateur().updateQteArme(listeRetirerArme)
        DAOUtilisateur().updateArmeUtilisateur(self.joueur1.getTousLesUtilisateurs()[0]["ID"],self.joueur1)
        DAOUtilisateur().updateArmeUtilisateur(self.joueur2.getTousLesUtilisateurs()[0]["ID"],self.joueur2)
        self.menuChoixNiveau = InterfaceMenuChoixMap(self.joueur1,self.joueur2,listeArmeJ1,listeArmeJ2,self.cj1,self.cj2)