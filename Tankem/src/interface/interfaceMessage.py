## -*- coding: utf-8 -*-
from util import *

from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
import random
from Internal.DAO.DAOUtilisateur import DAOUtilisateur
from Internal.DAO.DAOEquipement import DAOEquipement
from Internal.DAO.DAOHeatmap import DAOHeatmap
from Internal.DAO.DAOStatArmes import DAOStatArmes
from Internal.DAO.DAOPartie import DAOPartie
from map import Map
import time
 
class InterfaceMessage(ShowBase):
    def __init__(self,j1,j2,laj1,laj2,carte,objmap):
        self.accept("tankElimine",self.displayGameOver)

        self.debut=0
        self.fin=0

        self.j1=j1
        self.j2=j2
        self.objmap=objmap
        self.carteid=carte["id"]
        self.vieMaxTank=self.objmap.listTank[0].pointDeVieMax
        
        self.callBackFunction = None

        self.accept("showHelp",self.displayHelp)

        self.createHelpText()
        self.displayHelp(False)

    def effectCountDownStart(self,nombre,callbackFunction):
        self.callBackFunction = callbackFunction
        self.displayCountDown(nombre)
        
    def displayCountDown(self, nombre):
        message = str(nombre)
        startScale = 0.4

        text = TextNode('Compte à rebour')
        text.setText(message)
        textNodePath = aspect2d.attachNewNode(text)
        textNodePath.setScale(startScale)
        text.setShadow(0.05, 0.05)
        text.setShadowColor(0, 0, 0, 1)
        text.setTextColor(0.5, 0.5, 1, 1)
        text.setAlign(TextNode.ACenter)

        effetScale = LerpScaleInterval(textNodePath, 1.0, 0.05, startScale)
        effetFadeOut = LerpColorScaleInterval(textNodePath, 1.0, LVecBase4(1,1,1,0), LVecBase4(1,1,1,1))
        effetFadeOut.start()

        recursion = Func(self.displayCountDown,nombre-1)

        #Le prochain tour, on affiche la message de début de partie
        if(nombre == 1):
            recursion = Func(self.displayStartGame)
        sequence = Sequence(effetScale,recursion)
        sequence.start()

    def displayStartGame(self):
        message = "Tankem!"
        startScale = 0.4

        text = TextNode('Début de la partie')
        text.setText(message)
        textNodePath = aspect2d.attachNewNode(text)
        textNodePath.setScale(startScale)
        text.setShadow(0.05, 0.05)
        text.setShadowColor(0, 0, 0, 1)
        text.setTextColor(0.5, 0.5, 1, 1)
        text.setAlign(TextNode.ACenter)

        delai = Wait(0.3)
        effetFadeOut = LerpColorScaleInterval(textNodePath, 0.15, LVecBase4(1,1,1,0), LVecBase4(1,1,1,1), blendType = 'easeIn')

        sequence = Sequence(delai,effetFadeOut,Func(self.callBackFunction))
        sequence.start()

    def displayGameOver(self, idPerdant):

        self.fin=time.time()

        self.heatmapG=self.objmap.hm

        ## on va mettre le nom du jouer en param
        if (idPerdant==1):
            nom=self.j1.getTousLesUtilisateurs()[0]["NOM_UTILISATEUR"]
            self.victoire(self.j1,self.objmap.listTank[0].pointDeVie)
            self.stats(self.j1.getTousLesUtilisateurs()[0]["ID"])
        if (idPerdant==0):
            nom=self.j2.getTousLesUtilisateurs()[0]["NOM_UTILISATEUR"]
            self.victoire(self.j2,self.objmap.listTank[1].pointDeVie)
            self.stats(self.j2.getTousLesUtilisateurs()[0]["ID"])

        message = "\n" + nom + " a gagné!"
        startScale = 0.3

        text = TextNode('Annonce game over')
        text.setText(message)
        textNodePath = aspect2d.attachNewNode(text)
        textNodePath.setScale(startScale)
        textNodePath.setColorScale(LVecBase4(1,1,1,0))
        text.setShadow(0.05, 0.05)
        text.setShadowColor(0, 0, 0, 1)
        text.setTextColor(0.01, 0.2, 0.7, 1)
        text.setAlign(TextNode.ACenter)

        delai = Wait(0.5)
        effetFadeIn = LerpColorScaleInterval(textNodePath, 1, LVecBase4(1,1,1,1), LVecBase4(1,1,1,0), blendType = 'easeIn')

        sequence = Sequence(delai,effetFadeIn)
        sequence.start()

        #bouton pour ajouter au favoris
        self.favorisJ1 = DirectButton(text = (u"Ajouter carte au favoris j1", "Ajouter carte au favoris j1", "Ajouter carte au favoris j1", "disabled"),
                        scale=.1,
                        pos=(-.8,0,-0.7),
                        command=self.ajouterFavoris,
                        extraArgs=["j1"])

        self.favorisJ2 = DirectButton(text = (u"Ajouter carte au favoris j2", "Ajouter carte au favoris j2", "Ajouter carte au favoris j2", "disabled"),
                        scale=.1,
                        pos=(.8,0,-0.7),
                        command=self.ajouterFavoris,
                        extraArgs=["j2"])

    def stats(self,idgagnant):
        # on va get les info des arme pour les deux joueur
        self.j1_infoArme=self.objmap.listTank[0].dtoArme
        self.j1_infoArme.setIdJoueur(self.j1.getTousLesUtilisateurs()[0]["ID"])
        self.heatmapG.joueur1.setIdJoueur(self.j1.getTousLesUtilisateurs()[0]["ID"])

        self.j2_infoArme=self.objmap.listTank[1].dtoArme
        self.j2_infoArme.setIdJoueur(self.j2.getTousLesUtilisateurs()[0]["ID"])
        self.heatmapG.joueur2.setIdJoueur(self.j2.getTousLesUtilisateurs()[0]["ID"])

        DAOStatArmes().ecireDansDB(self.j1_infoArme)
        DAOStatArmes().ecireDansDB(self.j2_infoArme)
        DAOHeatmap().ecrireDansDB(self.heatmapG)
        DAOPartie().updaterPartie(self.j1.getTousLesUtilisateurs()[0]["ID"],self.j2.getTousLesUtilisateurs()[0]["ID"],idgagnant,self.heatmapG.idPartie)

    def ajouterFavoris(self,joueur):
        if(joueur=="j1"):
            DAOUtilisateur().updateAjouterAuFavoris(self.j1.getTousLesUtilisateurs()[0]["ID"],self.carteid)
            self.favorisJ1.hide()
        if(joueur=="j2"):
            DAOUtilisateur().updateAjouterAuFavoris(self.j2.getTousLesUtilisateurs()[0]["ID"],self.carteid)
            self.favorisJ2.hide()

    def victoire(self,joueur,pointVie):
        listeArme=["Grenade","Mitraillette","Piege","Shotgun","Missile","Spring"]
        listeArmeActuel=[]

        listeArmeId=DAOEquipement().lireDB()

        for arme in joueur.getTousLesUtilisateurs()[0]["EQUIPEMENT"]:
          if(arme!="Canon"):
            idArme = joueur.getTousLesUtilisateurs()[0]["EQUIPEMENT"][arme]["ARME"]["ID"]
            idUtil = joueur.getTousLesUtilisateurs()[0]["ID"]
            qte = joueur.getTousLesUtilisateurs()[0]["EQUIPEMENT"][arme]["QTE"]
            listeArmeActuel.append([idArme,idUtil,qte])

        #nouvelle liste des arme quil gagne
        listeArmeDonner=[]

        #gagnant +1 arme
        listeArmeDonner.append(random.choice(listeArme))

        #partie de moins de une minute +1
        if((self.fin-self.debut)<60.0):
            listeArmeDonner.append(random.choice(listeArme))

        #100% vie +3 arme
        if (self.vieMaxTank==pointVie):
            for i in range(0,3):
                listeArmeDonner.append(random.choice(listeArme))

        #plus de 50% vie +1 arme
        if(pointVie>(self.vieMaxTank/2)):
            listeArmeDonner.append(random.choice(listeArme))

        listeNouvelleArme=[]
        listeAncienneArme=[]

        for arme in listeArmeDonner:
            dejaLa = False
            for i in range(0,len(listeArmeActuel)):
                armeW = listeArmeActuel[i]
                idB = -1
                for i in range(0,len(listeArmeId.getTousLesArmes())):
                    if arme == listeArmeId.getTousLesArmes()[i]["NOM"]:
                        idB=listeArmeId.getTousLesArmes()[i]["ID"]
                        break
                idArmeD = listeArmeId.getTousLesArmes()[i]["ID"]
                if idB == armeW[0]:
                    dejaLa=True
                    break
            for i in range(0,len(listeArmeId.getTousLesArmes())):
                    if arme ==listeArmeId.getTousLesArmes()[i]["NOM"]:
                        idA=listeArmeId.getTousLesArmes()[i]["ID"]
                        break
            if dejaLa==True:
                listeAncienneArme.append([idA,joueur.getTousLesUtilisateurs()[0]["ID"],joueur.getTousLesUtilisateurs()[0]["EQUIPEMENT"][arme]["QTE"]])
            if dejaLa == False:
                d = False
                for j in listeNouvelleArme:
                    if j[0] == idA:
                        d=True
                        break
                if d == True:
                    listeAncienneArme.append([idA,joueur.getTousLesUtilisateurs()[0]["ID"],1])
                else:
                    listeNouvelleArme.append([idA,joueur.getTousLesUtilisateurs()[0]["ID"],1])

        DAOUtilisateur().updateQteArmeFinPartie(listeNouvelleArme,listeAncienneArme)

    def effectMessageGeneral(self, message, duration):
        text = TextNode('Message general')

        startScale = 0.12
        text.setText(message)
        textNodePath = aspect2d.attachNewNode(text)
        textNodePath.setScale(startScale)
        textNodePath.setColorScale(LVecBase4(1,1,1,0))
        text.setShadow(0.05, 0.05)
        text.setShadowColor(0, 0, 0, 1)
        text.setTextColor(0.01, 0.2, 0.7, 1)
        text.setAlign(TextNode.ACenter)
        textNodePath.setPos(Vec3(0,0,0.65))

        delai = Wait(duration)
        effetFadeIn = LerpColorScaleInterval(textNodePath, 0.3, LVecBase4(1,1,1,1), LVecBase4(1,1,1,0), blendType = 'easeIn')
        effetFadeOut = LerpColorScaleInterval(textNodePath, 0.3, LVecBase4(1,1,1,0), LVecBase4(1,1,1,1), blendType = 'easeIn')

        sequence = Sequence(effetFadeIn,delai,effetFadeOut)
        sequence.start()

    def displayHelp(self,mustShow):
        self.textNodePath.show() if mustShow else self.textNodePath.hide()

    def createHelpText(self):
        #TODO: touches sont dupliquées dans le inputManager et ici. On doit centraliser
        message = """Contrôle\n
        Contrôle avec la souris: F2\n
        ----Joueur 1----\n
        Bouger: wasd\n
        Tirer arme principale: v\n
        Tirer arme secondaire: b\n
        Détonation des balles: b\n
        \n
        ----Joueur 2----\n
        Bouger: flèches\n
        Tirer arme principale: NumPad-1\n
        Tirer arme secondaire: NumPad-2\n
        Détonation des balles: NumPad-3\n\n
        """

        text = TextNode('Aide')
        text.setText(message)
        self.textNodePath = aspect2d.attachNewNode(text)
        self.textNodePath.setScale(0.055)
        self.textNodePath.setColorScale(LVecBase4(1,1,1,1))
        text.setShadow(0.05, 0.05)
        text.setShadowColor(0, 0, 0, 1)
        text.setTextColor(0.01, 0.2, 0.1, 1)
        text.setAlign(TextNode.ALeft)
        self.textNodePath.setPos(Vec3(-1.65,0,0.65))