ReadMe: Phase2 Tankem

aller dans \Tools\InstallationBCrypt
lancer installBCrypt.bat

--------------------------------------------------------------------------------------
Lancer une partie:
--------------------------------------------------------------------------------------

Pour lancer le jeu, d�marrer luncher.bat, choisir Jouer.
Le joueur arrive alors sur l'interface de choix de map.
Il peut alors choisir de lancer une des map dans la liste (cliquer sur une map,
ses informations s'affichent, puis "lancer le jeu"). Ou bien lancer directement la map 
par d�faut, sans cliquer pr�alablement sur une map dans la liste.

--------------------------------------------------------------------------------------
Editeur de niveau
--------------------------------------------------------------------------------------

Si le joueur choisi l'option "Editeur de niveau" dans l'interface, il est redirig�
vers l'interface de cr�ation de map. Il peut dans cet �cran choisir la taille de la map
en hauteur/largeur, naviguer entre les diff�rentes tuiles avec les touches directionnelles
changer l'�tat des tuiles avec les touches suivantes:

a -> Case pr�c�dente
s -> Case suivante
z -> Placer joueur 1
x -> Placer joueur 2
w -> Changer etat

Apr�s avoir enregistr� la carte, l'utilisateur revient au menu de choix de carte et
la carte cr�ee s'ajoute � la liste des cartes disponibles afin de lancer une partie.

